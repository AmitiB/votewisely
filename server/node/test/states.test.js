const chai = require('chai');

const { expect } = chai;
const sinon = require('sinon');
const states = require('../routes/states');

describe('States', () => {
  it('stateInfoSQLGenerator should use abbrev params to generate a SQL statement to get information about a state', () => {
    const mockReq = sinon.mock();
    mockReq.params = sinon.mock();
    mockReq.params.abbrev = 'TX';
    const [testSQL, testSQLargs] = states.stateInfoSQLGenerator(mockReq);
    expect(testSQL).to.equal('SELECT * FROM states_table WHERE abbrev=?;');
    expect(testSQLargs).to.be.an('array');
    expect(testSQLargs).to.have.length(1);
    expect(testSQLargs[0]).to.equal('TX');
  });

  it('statesSQLGenerator should generate a SQL statement to get information about all states', () => {
    const [testSQL, testSQLargs] = states.statesSQLGenerator();
    expect(testSQL).to.equal('SELECT * FROM states_table;');
    expect(testSQLargs).to.be.empty;
  });
});
