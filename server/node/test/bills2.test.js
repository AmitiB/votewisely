const chai = require('chai');

const { expect } = chai;
const sinon = require('sinon');
const bills2 = require('../routes/bills2');

describe('Bills', () => {
  it('billsSQLGenerator should return a SQl statement and its arguments', () => {
    const mockRes = sinon.mock();
    mockRes.query = sinon.mock();
    mockRes.query.bill_id = 'bill_test_123';
    mockRes.query.state = 'TX';
    mockRes.query.sponsor_id = 'sponsor_test_123';
    const [testSQL, testSQLargs] = bills2.billsSQLGenerator(mockRes);
    expect(testSQL).to.equal(
      'SELECT * FROM bills_table_2 WHERE bill_id=? AND sponsor_state=? AND sponsor_id=? ',
    );
    expect(testSQLargs).to.be.an('array');
    expect(testSQLargs).to.have.length(3);
    expect(testSQLargs[0]).to.be.equal('bill_test_123');
    expect(testSQLargs[1]).to.be.equal('TX');
  });
});
