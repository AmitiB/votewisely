const chai = require('chai');

const { expect } = chai;
const sinon = require('sinon');
const bills2issues = require('../routes/bills2issues');

describe('Bills to Issues', () => {
  it('bills2issuesSQLGenerator should generate a SQL statement without arguments when request has no arguments', () => {
    const mockQuery = sinon.mock();
    mockQuery.query = sinon.mock();
    const [testSQL, testSQLargs] = bills2issues.bills2issuesSQLGenerator(mockQuery);
    expect(testSQL).to.equal('SELECT * FROM bills_to_issues;');
    expect(testSQLargs).to.be.empty;
  });

  it('bills2issuesSQLGenerator should generate a SQL statement with arguments when request has arguments', () => {
    const mockQuery = sinon.mock();
    mockQuery.query = sinon.mock();
    mockQuery.query.bill_id = 'testId';
    const [testSQL, testSQLargs] = bills2issues.bills2issuesSQLGenerator(mockQuery);
    expect(testSQL).to.equal('SELECT * FROM bills_to_issues WHERE bill_id=?;');
    expect(testSQLargs).to.be.an('array');
    expect(testSQLargs).to.have.length(1);
    expect(testSQLargs[0]).to.equal('testId');
  });
});
