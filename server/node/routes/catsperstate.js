const express = require('express');
const helpers = require('../lib/sqlHelpers');

const router = express.Router();

const TABLE = 'states_issue_bills';
const ISSUES_TABLE = 'issues_table_2';

function statesSQLGenerator() {
  const args = [];
  const sql = `SELECT DISTINCT state FROM ${TABLE}`;
  return [sql, args];
}

function statesProcessResults(results) {
  return results.map(obj => obj.state);
}

/* Request example: /api/catsperstate */
/* Returns possible queriable states */
router.get('/', helpers.sqlRequestFactory(statesSQLGenerator, statesProcessResults));

function catsPerStateSQLGenerator(req) {
  const args = [];
  const sql = `${'SELECT category, SUM(count) as count FROM (SELECT issues.category, count '
  + 'FROM '}${TABLE} `
  + `LEFT JOIN (SELECT name, category FROM ${ISSUES_TABLE}) AS issues ON issues.name=${TABLE}.issue `
  + 'WHERE state=?'
+ ') AS state_stats GROUP BY category';
  args.push(req.params.state);
  return [sql, args];
}

/* Return bill issue categories count for a state */
/* Request example: /api/catsperstate/TX  */
router.get('/:state', helpers.sqlRequestFactory(catsPerStateSQLGenerator));

module.exports = {
  statesSQLGenerator,
  catsPerStateSQLGenerator,
  router,
};
