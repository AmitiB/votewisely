const express = require('express');
const helpers = require('../lib/sqlHelpers');

const router = express.Router();

const TABLE = 'reps_issue_bills';
const REPS_TABLE = 'congress_members';

function issuesPerRepSQLGenerator() {
  const args = [];
  const sql = `${'SELECT reps_table.full_name as rep_name, COUNT(issue) as issues, reps_table.party '
    + 'FROM '}${TABLE} `
    + `LEFT JOIN (\
        SELECT id, full_name, party FROM ${REPS_TABLE}\
      ) AS reps_table ON ${TABLE}.rep_id=reps_table.id `
    + 'GROUP BY rep_id';
  return [sql, args];
}

/* Request example: /api/issuesperrep */
/* Returns possible queriable states */
router.get('/', helpers.sqlRequestFactory(issuesPerRepSQLGenerator));

module.exports = {
  issuesPerRepSQLGenerator,
  router,
};
