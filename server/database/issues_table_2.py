from bs4 import BeautifulSoup as bs
import json
import requests
import re
import os
import googleapiclient.discovery
import googleapiclient.errors

from azure.cognitiveservices.search.imagesearch import ImageSearchAPI
from msrest.authentication import CognitiveServicesCredentials

subscription_key = ""
youtube_api_key = ""

import table_api as sql

table_name = "issues_table_2"
table_schema = {
    "route": ("VARCHAR(256)", "NOT NULL"),
    "name": ("VARCHAR(256)", "NOT NULL"),
    "src": ("VARCHAR(1024)", "NOT NULL"),
    "category": ("VARCHAR(256)", "NOT NULL"),
    "votes_yes": ("INT", "NOT NULL", "DEFAULT 0"),
    "votes_no": ("INT", "NOT NULL", "DEFAULT 0"),
    "votes_total": ("INT", "NOT NULL", "DEFAULT 0"),
    "pie": ("VARCHAR(256)", "NOT NULL"),
    "side_yes": ("TEXT", "NOT NULL"),
    "side_no": ("TEXT", "NOT NULL"),
    "side_center": ("TEXT", "NOT NULL"),
    "summary": ("TEXT", "NOT NULL"),
    "question": ("VARCHAR(1024)", "NOT NULL"),
    "icon": ("VARCHAR(1024)", "NOT NULL"),
    "img1": ("VARCHAR(1024)", "NULL"),
    "img2": ("VARCHAR(1024)", "NULL"),
    "banner": ("VARCHAR(1024)", "NULL"),
    "youtube_id1": ("VARCHAR(1024)", "NULL"),
    "youtube_title1": ("VARCHAR(1024)", "NULL"),
    "youtube_desc1": ("VARCHAR(1024", "NULL"),
    "youtube_id2": ("VARCHAR(1024)", "NULL"),
    "youtube_title2": ("VARCHAR(1024)", "NULL"),
    "youtube_desc2": ("VARCHAR(1024", "NULL"),
    "bill_1": ("CHAR(20)", "NULL", "DEFAULT NULL"),
    "bill_2": ("CHAR(20)", "NULL", "DEFAULT NULL"),
    "bill_3": ("CHAR(20)", "NULL", "DEFAULT NULL"),
    "bill_4": ("CHAR(20)", "NULL", "DEFAULT NULL"),
    "bill_5": ("CHAR(20)", "NULL", "DEFAULT NULL"),
    "statement_1": ("INT", "NULL", "DEFAULT NULL"),
    "statement_2": ("INT", "NULL", "DEFAULT NULL"),
    "statement_3": ("INT", "NULL", "DEFAULT NULL"),
    "statement_4": ("INT", "NULL", "DEFAULT NULL"),
    "statement_5": ("INT", "NULL", "DEFAULT NULL"),
}
primary_key = "route"
foreign_keys = {
    "bill_1": "bills_table_2(bill_id)",
    "bill_2": "bills_table_2(bill_id)",
    "bill_3": "bills_table_2(bill_id)",
    "bill_4": "bills_table_2(bill_id)",
    "bill_5": "bills_table_2(bill_id)",
    "statement_1": "statements_table(id)",
    "statement_2": "statements_table(id)",
    "statement_3": "statements_table(id)",
    "statement_4": "statements_table(id)",
    "statement_5": "statements_table(id)",
}
table_constraints = {
    "issues_table_2_route_uindex": f"UNIQUE ({primary_key})",
    "pk_route": f"PRIMARY KEY ({primary_key})",
}


def get_request(url):
    r = requests.get(url)
    if r.status_code != 200:
        raise Exception(f"GET /tasks/ {r.status_code}")
    else:
        print(f"Request successful")
    return r.content


def insert_issue(issue_dict):
    existing_issue = sql.sql_fetch_all(
        f"SELECT votes_total FROM {table_name} "
        f"WHERE {primary_key}='{issue_dict[primary_key]}'"
    )
    if existing_issue:
        print(f"Duplicate {issue_dict[primary_key]} found")
        if existing_issue[0][0] < issue_dict["votes_total"]:
            print("Found more info, replacing...")
            sql.insert_table(table_name, table_schema, issue_dict, primary_key)
        else:
            print("Ignoring...")
    else:
        sql.insert_table(table_name, table_schema, issue_dict, primary_key)


def update_issue_bills(route, bills):
    issue_dict = {
        "route": route,
        "bill_1": None if len(bills) < 1 else bills[0],
        "bill_2": None if len(bills) < 2 else bills[1],
        "bill_3": None if len(bills) < 3 else bills[2],
        "bill_4": None if len(bills) < 4 else bills[3],
        "bill_5": None if len(bills) < 5 else bills[4],
    }
    sql.insert_table(table_name, table_schema, issue_dict, primary_key)


def update_issue_statements(route, statements):
    issue_dict = {
        "route": route,
        "statement_1": None if len(statements) < 1 else statements[0],
        "statement_2": None if len(statements) < 2 else statements[1],
        "statement_3": None if len(statements) < 3 else statements[2],
        "statement_4": None if len(statements) < 4 else statements[3],
        "statement_5": None if len(statements) < 5 else statements[4],
    }
    sql.insert_table(table_name, table_schema, issue_dict, primary_key)


def update_issue_imgs(route, imgs):
    issue_dict = {"route": route, "img1": imgs[0], "img2": imgs[1], "banner": imgs[2]}
    sql.insert_table(table_name, table_schema, issue_dict, primary_key)


def insert_issues_imgs():
    issues = sql.sql_fetch_all(f"SELECT {primary_key}, name FROM {table_name}")
    client = ImageSearchAPI(CognitiveServicesCredentials(subscription_key))
    for route, name in issues:
        image_results = client.images.search(query=name, size="large")
        print(f"Search images for {name} results that are large sized.")
        print(f"Image result count: {len(image_results.value)}")
        images = [image_results.value[i].content_url for i in range(3)]
        update_issue_imgs(route, images)


def set_up_youtube_api_client():
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    api_service_name = "youtube"
    api_version = "v3"
    DEVELOPER_KEY = youtube_api_key

    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey=DEVELOPER_KEY
    )
    return youtube


def get_youtube_api_response(search_keyword, youtube):
    request = youtube.search().list(
        part="snippet",
        maxResults=2,
        order="viewCount",
        publishedAfter="2013-01-01T00:00:00Z",
        q="What is " + search_keyword,
        safeSearch="moderate",
    )
    response = request.execute()
    return response


def update_issue_youtube_ids(route, youtube_info):
    issue_dict = {
        "route": route,
        "youtube_id1": youtube_info[0],
        "youtube_title1": youtube_info[1],
        "youtube_desc1": youtube_info[2],
        "youtube_id2": None if len(youtube_info) <= 3 else youtube_info[3],
        "youtube_title2": None if len(youtube_info) <= 4 else youtube_info[4],
        "youtube_desc2": None if len(youtube_info) <= 5 else youtube_info[5],
    }
    sql.insert_table(table_name, table_schema, issue_dict, primary_key)


def insert_issues_youtube_ids():
    issues = sql.sql_fetch_all(f"SELECT {primary_key}, name FROM {table_name}")
    youtube = set_up_youtube_api_client()
    go = False
    for route, name in issues:
        if route == "voter_fraud":
            go = True
        if go:
            youtube_info = []
            res = get_youtube_api_response(name, youtube)
            for i in range(min(len(res["items"]), 2)):
                youtube_info.append(res["items"][i]["id"]["videoId"])
                youtube_info.append(res["items"][i]["snippet"]["title"])
                youtube_info.append(res["items"][i]["snippet"]["description"])
            print(
                f"Updating youtube info for {name}...{len(res['items'])} results found.)"
            )
            update_issue_youtube_ids(route, youtube_info)


def insert_issues():
    url = "https://www.isidewith.com/polls"
    soup = bs(get_request(url), features="html.parser")
    categories = soup.find_all("h2")
    for category_html in categories:
        match = re.match("[a-zA-Z0-9 ]*(?=( Issues$))", category_html.text)
        if match:
            category = match.group(0)
            print(f'Getting information for "{category}" category...')
            category_issues = category_html.find_next("div", class_="polls").find_all(
                "div", class_="poll"
            )
            for issue in category_issues:
                issue_info = issue.find_all("p")
                issue_name = issue_info[0].text
                issue_route = issue_name.lower().replace(" ", "_")
                print(f'Getting information for "{issue_name}" issue...')
                issue_src = "https://www.isidewith.com" + issue.a["href"]
                issue_content = bs(get_request(issue_src), features="html.parser")
                issue_votes = issue_content.find("div", class_="yes_or_no")
                issue_side_yes = []
                for side_html in issue_content.find_all(
                    "div", class_="answer side_yes"
                ):
                    side_perc = side_html.find("p", class_="perc")
                    side_perc.find("small").decompose()
                    side_perc = int(side_perc.text)
                    issue_side_yes.append(
                        {
                            "perc": side_perc,
                            "comment": side_html.find("p", class_="title").text.replace(
                                "\xA0", " "
                            ),
                        }
                    )
                issue_side_no = []
                for side_html in issue_content.find_all("div", class_="answer side_no"):
                    side_perc = side_html.find("p", class_="perc")
                    side_perc.find("small").decompose()
                    side_perc = int(side_perc.text)
                    issue_side_no.append(
                        {
                            "perc": side_perc,
                            "comment": side_html.find("p", class_="title").text.replace(
                                "\xA0", " "
                            ),
                        }
                    )
                issue_side_center = []
                for side_html in issue_content.find_all(
                    "div", class_="answer side_center"
                ):
                    side_perc = side_html.find("p", class_="perc")
                    side_perc.find("small").decompose()
                    side_perc = int(side_perc.text)
                    issue_side_center.append(
                        {
                            "perc": side_perc,
                            "comment": side_html.find("p", class_="title").text.replace(
                                "\xA0", " "
                            ),
                        }
                    )
                issue_desc = issue_content.find("div", id="learn_more")
                if issue_desc:
                    issue_desc = issue_desc.p
                    issue_desc.find("a").decompose()
                    issue_desc = issue_desc.text.strip().replace("\xA0", " ")
                    issue_dict = {
                        "route": issue_route,
                        "name": issue_name,
                        "category": category,
                        "src": issue_src,
                        "question": issue_info[1].text,
                        "votes_yes": int(
                            issue_votes.find("div", class_="yes")
                            .find("p", class_="count")
                            .text.split(" ")[0]
                            .replace(",", "")
                        ),
                        "votes_no": int(
                            issue_votes.find("div", class_="no")
                            .find("p", class_="count")
                            .text.split(" ")[0]
                            .replace(",", "")
                        ),
                        "votes_total": int(
                            issue.find("div", class_="count")
                            .text.split(" ")[0]
                            .replace(",", "")
                        ),
                        "pie": "https://www.isidewith.com" + issue_votes.img["src"],
                        "side_yes": json.dumps(issue_side_yes),
                        "side_no": json.dumps(issue_side_no),
                        "side_center": json.dumps(issue_side_center),
                        "summary": issue_desc,
                        "icon": "https:" + issue.img["src"],
                    }
                    insert_issue(issue_dict)
                else:
                    print(f"Ignoring {issue_name} due to no description")


if __name__ == "__main__":
    sql.create_table(table_name, table_schema, foreign_keys, table_constraints)
    insert_issues_youtube_ids()
