import mysql.connector
from mysql.connector import errorcode
import re
import requests
import json
from bs4 import BeautifulSoup

from issues_config import issues_config
from apikey import apikey

config = {
    "user": "votewisely",
    "password": "votewisely123",
    "host": "cs373votewisely.cqqkkpkbowt8.us-east-1.rds.amazonaws.com",
    "port": "3306",
    "database": "VoteWiselyDB",
    "raise_on_warnings": True,
}


def get_json_request(url, headers=None):
    """
    Returns the requested json from a URL
    :return:
    """
    res = requests.get(url=url, headers=headers)
    if res.status_code != 200:
        raise Exception(f"GET /tasks/ {res.status_code}")
    else:
        print("API GET request successful!")
    return res.json()


def get_wikipedia_intro(page):
    """
    Gets up to 3 paragraphs from the intro of a wikipedia page.
    :return:
    """
    url = f"https://en.wikipedia.org/w/api.php?format=json&action=query&titles={page}&prop=extracts&exintro&explaintext"
    response = get_json_request(url)
    extract = next(iter(response["query"]["pages"].values()))["extract"]
    return "\n".join(extract.split("\n")[:3])


def get_wikipedia_section(page, section, sliced):
    """
    Gets slice of paragraphs of a provided section in wikipedia.
    :return:
    """
    sections_url = f"https://en.wikipedia.org/w/api.php?action=parse&format=json&page={page}&prop=sections"
    response = get_json_request(sections_url)
    section_num = None
    for i in response["parse"]["sections"]:
        if i["anchor"] == section:
            section_num = i["index"]
    if not section_num:
        print(f"There are no section {section} for {page} page on wikipedia.")
        return None

    section_url = f"https://en.wikipedia.org/w/api.php?action=parse&format=json&page={page}&section={section_num}&prop=text"
    response = get_json_request(section_url)
    html = response["parse"]["text"]["*"]
    soup = BeautifulSoup(html, "html.parser")
    paragraphs = soup.get_text().split("\n")

    # That great what the heck Wikipedia filtering
    def beginning_match(s, iterable):
        for match in iterable:
            if s[: len(match)] == match:
                return True

    paragraphs = filter(lambda s: s != "", paragraphs)
    paragraphs = filter(lambda s: not s[0].isdigit(), paragraphs)
    paragraphs = filter(lambda s: not "[edit]" in s, paragraphs)
    paragraphs = filter(
        lambda s: not beginning_match(s, ["See also", "Main article", "Contents"]),
        paragraphs,
    )

    paragraphs = list(paragraphs)[sliced]
    parsed = list(map(lambda s: re.sub("\\[[0-9]+\\]", "", s), paragraphs))
    return "\n".join(parsed)


def dict_select(d, *args):
    """
    Select values in d with values in *args as the key
    :return:
    """
    return {key: d[key] for key in args if key in d}


def update_bill_info(props):
    """
    :param props: include a dictionary of attributes that needed to update or insert a bill entry
    :return: 0 if success
    """
    attributes = (
        "bill_id",
        "issue_name",
        "number",
        "bill_uri",
        "title",
        "sponsor_title",
        "bill_type",
        "sponsor_id",
        "sponsor_name",
        "sponsor_state",
        "sponsor_party",
        "introduced_date",
        "committees",
        "primary_subject",
        "summary",
        "summary_short",
        "congressdotgov_url",
    )
    add_bill = (
        f"INSERT INTO bills_table "
        "(bill_id, issue_name, number, bill_uri, title, sponsor_title, bill_type, sponsor_id, sponsor_name, sponsor_state, sponsor_party, introduced_date, committees, primary_subject, summary, summary_short, congressdotgov_url) "
        "VALUES (%(bill_id)s, %(issue_name)s, %(number)s, %(bill_uri)s, %(title)s, %(sponsor_title)s, %(bill_type)s, %(sponsor_id)s, %(sponsor_name)s, %(sponsor_state)s, %(sponsor_party)s, %(introduced_date)s, %(committees)s, %(primary_subject)s, %(summary)s, %(summary_short)s, %(congressdotgov_url)s) "
        "ON DUPLICATE KEY UPDATE bill_id=bill_id"
    )

    # just an example, set additional attributes by following the same format
    """
    update_bill = (
        f"UPDATE {table} "
        "SET issue_name=%(issue_name)s"
        "WHERE bill_id=%(bill_id)s"
    )
    """

    bill_data = dict_select(props, *attributes)
    cursor.execute(add_bill, bill_data)
    cnx.commit()
    return 0


def get_bills_search(query, offset=0):
    """
    Get first 5 bills id related to query by search.
    :return:
    """
    headers = {"X-API-Key": apikey}
    issue_name = query
    query = query.replace(" ", "%20")
    query = query.replace('"', "%22")
    url = f"https://api.propublica.org/congress/v1/bills/search.json?query={query}&offset={offset}"

    response = get_json_request(url, headers)
    if response["status"] != "OK":
        print(response)
        return None
    bills = response["results"][0]["bills"][:5]
    for bill in bills:
        bill["issue_name"] = query
        bill["congressdotgov_url"] = get_json_request(bill["bill_uri"], headers)[
            "results"
        ][0]["congressdotgov_url"]
        update_bill_info(bill)
    bills = [dict_select(obj, "bill_id") for obj in bills]
    return json.dumps(bills, separators=(",", ":"))


def get_statements_search(query, offset=0):
    """
    Get first 5 statements related to query by search.
    :return:
    """
    headers = {"X-API-Key": apikey}
    query = query.replace(" ", "%20")
    query = query.replace('"', "%22")
    url = f"https://api.propublica.org/congress/v1/statements/search.json?query={query}&offset={offset}"

    response = get_json_request(url, headers)
    if response["status"] != "OK":
        print(response)
        return None
    titles_added = set()
    statements = []
    for statement in response["results"]:
        if statement["title"] not in titles_added:
            titles_added.add(statement["title"])
            statements.append(statement)
            if len(statements) == 5:
                break
    statements = [
        dict_select(
            obj,
            "url",
            "date",
            "title",
            "statement_type",
            "member_id",
            "name",
            "state",
            "party",
            "subjects",
        )
        for obj in statements
    ]
    return json.dumps(statements, separators=(",", ":"))


def create_issues_table():
    table_description = (
        "  CREATE TABLE IF NOT EXISTS `issues_table` ("
        "   `name` VARCHAR(50) NOT NULL,"
        "   `description` MEDIUMTEXT NOT NULL,"
        "   `description_src` VARCHAR(255) NOT NULL,"
        "   `public_opinion` MEDIUMTEXT NOT NULL,"
        "   `public_opinion_src` VARCHAR(255) NOT NULL,"
        "   `bills` MEDIUMTEXT NOT NULL,"
        "   `statements` MEDIUMTEXT NOT NULL,"
        "   PRIMARY KEY (name)"
        ") ENGINE=InnoDB"
    )
    try:
        print(f"Creating table `issues_table`: ")
        cursor.execute(table_description)
    except mysql.connector.Error as e:
        if e.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(e.msg)
    else:
        print("OK")


def insert_issues(issues):
    table = "issues_table"
    attributes = (
        "name",
        "description",
        "description_src",
        "public_opinion",
        "public_opinion_src",
        "bills",
        "statements",
    )
    add_issue = (
        f"INSERT INTO {table} "
        "(name, description, description_src, public_opinion, public_opinion_src, bills, statements) "
        "VALUES (%(name)s, %(description)s, %(description_src)s, %(public_opinion)s, %(public_opinion_src)s, %(bills)s, %(statements)s)"
    )
    wikipedia = "https://en.wikipedia.org/wiki/"

    for issue in issues:
        entry = {}
        entry["name"] = issue
        page = issues_config[issue]["page"]
        entry["description_src"] = wikipedia + page
        entry["description"] = get_wikipedia_intro(page)
        page, section, num = issues_config[issue]["public_opinion"]
        entry["public_opinion_src"] = wikipedia + page + "#" + section
        entry["public_opinion"] = get_wikipedia_section(page, section, num)
        entry["bills"] = get_bills_search(issue)
        entry["statements"] = get_statements_search(issue)
        cursor.execute(add_issue, entry)
        print(f"Insertion of {issue} completed!")

    return 0  # success


try:
    cnx = mysql.connector.connect(**config)
    print("Connection successful!")
    cursor = cnx.cursor()
    if __name__ == "__main__":
        create_issues_table()

        insert_issues(issues_config)
        cnx.commit()  # only when you insert or update table


except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else:
        print(err)
else:
    cursor.close()
    cnx.close()
