import mysql.connector
from mysql.connector import errorcode
import requests

from issues_config import issues_config
from apikey import apikey

config = {
    "user": "votewisely",
    "password": "votewisely123",
    "host": "cs373votewisely.cqqkkpkbowt8.us-east-1.rds.amazonaws.com",
    "port": "3306",
    "database": "VoteWiselyDB",
    "raise_on_warnings": True,
}

issue_pages = {
    "Abortion": "abortion",
    "Climate%20Change": "climate",
    "Climate Change": "climate",
    "Euthanasia": "euthanasia",
    "Gun%20Politics": "gun",
    "Gun Politics": "gun",
    "Healthcare": "healthcare",
    "Immigration": "immigration",
    "LGBTQ": "lgbtq",
    "Capital%20Punishment": "punishment",
    "Capital Punishment": "punishment",
    "Minimum Wage": "wage",
}


def get_request(issue_topic):
    """
    Make GET API call to the ProPublica using API_KEY
    :return: JSON format of the request result
    """
    offset = 0
    sort = "_score"
    issue_topic = issue_topic.replace(" ", "%20")
    issue_topic = issue_topic.replace('"', "%22")
    request_url = f"https://api.propublica.org/congress/v1/bills/search.json?query={issue_topic}&offset={offset}&sort={sort}"

    params = {"X-API-Key": apikey}
    res = requests.get(url=request_url, headers=params)
    if res.status_code != 200:
        raise Exception(f"GET /tasks/ {res.status_code}")
    else:
        print(
            f"API GET request with query {issue_topic} and offset {offset} successful!"
        )
    return res.json()


def create_table(table):
    table_description = """
            create table if not exists bills_table
        (
            bill_id CHAR(20) not null,
            issue_name CHAR(64) null,
            number CHAR(20) not null,
            bill_uri CHAR(128) null,
            title VARCHAR(512) not null,
            sponsor_title CHAR(20) not null,
            bill_type CHAR(20) not null,
            sponsor_id CHAR(20) not null,
            sponsor_name CHAR(64) not null,
            sponsor_state CHAR(10) not null,
            sponsor_party CHAR(10) not null,
            introduced_date CHAR(20) not null,
            committees VARCHAR(256) not null,
            congressdotgov_url VARCHAR(256) not null,
            primary_subject CHAR(64) null,
            summary VARCHAR(4096) null,
            summary_short VARCHAR(1024) null
        );
        """
    # constraint bills_table_congress_members_id_fk
    #    foreign key (sponsor_id) references congress_members (id)
    create_index = (
        "create unique index bills_table_bill_id_uindex " "on bills_table (bill_id);"
    )

    alter_table = (
        "alter table bills_table "
        "add constraint bills_table_pk "
        "primary key (bill_id);"
    )

    try:
        print(f"Creating table {table}: ")
        cursor.execute(table_description)
        cursor.execute(create_index)
        cursor.execute(alter_table)
    except mysql.connector.Error as e:
        if e.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(e.msg)
    else:
        print("OK")


def update_table_issue_page_link(table):
    update_member = (
        f"UPDATE {table} "
        "SET issue_link=%(issue_link)s "
        "WHERE issue_name=%(issue_name)s"
    )
    for key, value in issue_pages.items():
        member_data = {"issue_name": key}
        member_data["issue_link"] = value
        cursor.execute(update_member, member_data)

    print("Update completed!")
    return 0


def insert_data(bills, table, issue_topic):
    """
    Insert the bill data into the database
    :param bills: a list of bills acquired from API call
    :param table: name of table to insert data into
    :return:
    """
    attributes = (
        "bill_id",
        "issue_name",
        "number",
        "bill_uri",
        "title",
        "sponsor_title",
        "bill_type",
        "sponsor_id",
        "sponsor_name",
        "sponsor_state",
        "sponsor_party",
        "introduced_date",
        "committees",
        "primary_subject",
        "summary",
        "summary_short",
        "congressdotgov_url",
    )
    add_bill = (
        f"INSERT INTO {table} "
        "(bill_id, issue_name, number, bill_uri, title, sponsor_title, bill_type, sponsor_id, sponsor_name, sponsor_state, sponsor_party, introduced_date, committees, primary_subject, summary, summary_short, congressdotgov_url) "
        "VALUES (%(bill_id)s, %(issue_name)s, %(number)s, %(bill_uri)s, %(title)s, %(sponsor_title)s, %(bill_type)s, %(sponsor_id)s, %(sponsor_name)s, %(sponsor_state)s, %(sponsor_party)s, %(introduced_date)s, %(committees)s, %(primary_subject)s, %(summary)s, %(summary_short)s, %(congressdotgov_url)s)"
        "ON DUPLICATE KEY UPDATE bill_id=bill_id"
    )
    i = 0
    for bill in bills:
        if i == 10:
            break
        bill_data = {key: bill[key] for key in attributes if key != "issue_name"}
        bill_data["issue_name"] = issue_topic
        cursor.execute(add_bill, bill_data)
        i += 1
    print(f"Insertion completed, row inserted {i}!")


try:
    cnx = mysql.connector.connect(**config)
    print("Connection successful!")
    cursor = cnx.cursor()
    table_name = "bills_table"

    if __name__ == "__main__":
        # create_table(table_name)

        # for issue in issues_config:
        #     r = get_request(issue)

        #     results = r["results"][0]
        #     print(f"Num results queried:{results['num_results']}")
        #     if results['num_results'] <= 10:
        #         raise Exception("Not enough results queried, try change the query keyword")

        #     insert_data(results["bills"], table_name, issue)
        #     cnx.commit()
        update_table_issue_page_link(table_name)
        cnx.commit()

except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else:
        print(err)
else:
    cursor.close()
    cnx.close()
