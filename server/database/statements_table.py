import json
import requests

from apikey import apikey
import demjson
import issues_table_2 as it
import table_api as sql

table_name = "statements_table"
table_schema = {
    "id": ("INT", "NOT NULL", "AUTO_INCREMENT"),
    "url": ("TEXT", "NOT NULL"),
    "date": ("CHAR(20)", "NOT NULL"),
    "title": ("TEXT", "NOT NULL"),
    "member_id": ("CHAR(20)", "NOT NULL"),
    "name": ("VARCHAR(256)", "NOT NULL"),
    "chamber": ("VARCHAR(256)", "NOT NULL"),
    "state": ("CHAR(20)", "NOT NULL"),
    "party": ("CHAR(20)", "NOT NULL"),
}
primary_key = "id"
foreign_keys = {"member_id": "congress_members(id)", "state": "states_table(abbrev)"}
table_constraints = {
    "statements_table_2_id_uindex": f"UNIQUE ({primary_key})",
    "pk_id": f"PRIMARY KEY ({primary_key})",
}

relation_table_name = "statements_to_issues"
relation_table_schema = {
    "id": ("INT", "NOT NULL"),
    "issue": ("VARCHAR(256)", "NOT NULL"),
}
relation_foreign_keys = {"id": "statements_table(id)", "issue": "issues_table_2(route)"}


def get_request(issue_topic, offset=0):
    """
    Make GET API call to the ProPublica using API_KEY
    :return: JSON format of the request result
    """
    sort = "_score"
    query_topic = issue_topic.replace(" ", "%20")
    query_topic = query_topic.replace('"', "%22")
    request_url = f"https://api.propublica.org/congress/v1/statements/search.json?query={query_topic}&offset={offset}&sort={sort}"

    params = {"X-API-Key": apikey}
    res = requests.get(url=request_url, headers=params)
    if res.status_code != 200:
        raise Exception(f"GET /tasks/ {res.status_code}")
    else:
        print(
            f"API GET request with query {issue_topic} and offset {offset} successful!"
        )
    return demjson.decode(res.content)["results"]


def insert_statement(statement_dict, issue):
    existing_statement = sql.sql_fetch_all(
        f"SELECT {primary_key} FROM {table_name} "
        f"WHERE title=\"{statement_dict['title']}\" AND date=\"{statement_dict['date']}\""
    )
    if existing_statement:
        print(f"Duplicate {statement_dict['title']} found")
        issues = sql.sql_fetch_all(
            f"SELECT issue FROM {relation_table_name} "
            f"WHERE {primary_key}={existing_statement[0][0]}"
        )
        issues = [issue[0] for issue in issues]
        if not issue in issues:
            sql.insert_table(
                relation_table_name,
                relation_table_schema,
                {"id": existing_statement[0][0], "issue": issue},
                None,
            )
        issues = sql.sql_fetch_all(
            f"SELECT issue FROM {relation_table_name} "
            f"WHERE {primary_key}={existing_statement[0][0]}"
        )
        print("Updating statement...")
    for key in statement_dict:
        if type(statement_dict[key]) != str and type(statement_dict[key]) != int:
            statement_dict[key] = json.dumps(statement_dict[key])
    sql.insert_table(table_name, table_schema, statement_dict, primary_key)
    if existing_statement:
        return existing_statement[0][0]
    new_id = sql.sql_fetch_all("SELECT LAST_INSERT_ID();")
    return new_id[0][0]


def insert_statements():
    """
    Insert the statements data into the database
    :return:
    """
    issues = sql.sql_fetch_all(f"SELECT {it.primary_key}, name FROM {it.table_name}")
    for issue, name in issues:
        statements = get_request(name)
        usable_statements = []
        for statement in statements:
            has_rep = sql.sql_fetch_all(
                f"SELECT id FROM congress_members WHERE id=\"{statement['member_id']}\" AND full_name=\"{statement['name']}\""
            )
            has_state = sql.sql_fetch_all(
                f"SELECT abbrev FROM states_table WHERE abbrev=\"{statement['state']}\""
            )
            if has_rep and has_state:
                statement[primary_key] = insert_statement(statement, issue)
                usable_statements.append(statement)
        print(f"Insertion {len(usable_statements)} statements for {name}!")
        issue_statements = list(
            set(
                [
                    usable_statements[i][primary_key]
                    for i in range(min(5, len(usable_statements)))
                ]
            )
        )
        it.update_issue_statements(issue, issue_statements)
        print(f"Updated statements for {issue} with {issue_statements}!")


if __name__ == "__main__":
    sql.create_table(table_name, table_schema, foreign_keys, table_constraints)
    sql.create_table(relation_table_name, relation_table_schema, relation_foreign_keys)
    insert_statements()
