/* Main Section for Issues in React */

// import React from 'react';

class IssueSection extends React.PureComponent {

  render() {
    return (
      <div id="card" class="container">
        <div class="image-cropper">
          <img src={this.props.image} id="selfie"/>
        </div>
        <div id="details">
          <h2>{this.props.topic}</h2>
          <p>{this.props.description}</p>
          <a href={this.props.page+".html"}>Learn More</a>
        </div>
      </div>
    );
  }

}

class IssueList extends React.PureComponent {

  render() {
    return (
      <div class="container" id="issues-list">

        <div class="row">
          <div class="col-xs-6 col-md-4 ">
            <IssueSection
              image="../imgs/immigration.jpg"
              topic="Immigration"
              description="Immigration deals with laws about how to legally enter
              the United States and how to handle those who enter illegally."
              page="immigration"
            />
          </div>
          <div class="col-xs-6 col-md-4 ">
            <IssueSection
              image="../imgs/abortion2.jpg"
              topic="Abortion"
              description="Abortion is a medical procedure resulting in the
              termination of a human pregnancy and death of a fetus."
              page="abortion"
            />
          </div>
          <div class="col-xs-6 col-md-4 ">
            <IssueSection
              image="../imgs/lgbtq2.jpg"
              topic="LGBTQ"
              description="LGBTQ laws center around same-sex marriage and privileges."
              page="lgbt"
            />
          </div>
        </div>
        <br></br>
      </div>

    );
  }

}

class Purpose extends React.PureComponent {
  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text banner-text">Issues</h2>
        <p id="overview">
          Current issues that are widely debated among the American public and prevelant topics
          during elections.
        </p>
      </div>
    );
  }
}

class Main extends React.PureComponent {
  render() {
    return (
      <div id="main-container" class="container">
        <img src="../imgs/issues.jpeg" id="banner" />
        <Purpose />
        <hr />
        <IssueList />
      </div>
    );
  }
}


ReactDOM.render(
  <Main />,
  document.getElementById('react-issues')
);
