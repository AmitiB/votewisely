/* Main Section for Representatives  in React
 * Imports RepMember Cards: member-card.js
 */

// import React from 'react';

class RepMember extends React.PureComponent {

  render() {
    return (
      <div id="card" class="container">
        <div class="image-cropper">
          <img src={this.props.image} id="selfie"/>
        </div>
        <div id="details">
          <a href={this.props.page+".html"}>{this.props.name}</a>
          <p>{this.props.party}</p>
          <a href={"https://www.twitter.com/"+this.props.twitter}>@{this.props.twitter}</a>
          <p>{this.props.phone}</p>
        </div>
      </div>
    );
  }

}

class RepMemberList extends React.PureComponent {

  render() {
    return (
      <div class="container" id="reps-list">
        <h2 class="title-text">Representatives</h2>


        <h3>Texas</h3>
        <br></br>
        <div class="row">
          <div class="col-xs-6 col-md-4 ">
            <RepMember
              image="http://www.cruz.senate.gov/files/images/OfficialPortrait.jpg" 
              name="Sen. Ted Cruz"
              page="persona"
              party="Republican"
              twitter="SenTedCruz"
              phone="(202) 224-5922"
            />
          </div>
          <div class="col-xs-6 col-md-4 ">
            <RepMember
              image="http://bioguide.congress.gov/bioguide/photo/C/C001056.jpg" 
              name="Sen. John Cornyn"
              party="Republican"
              page="persona"
              twitter="JohnCornyn"
              phone="(202) 224-2934"
            />
          </div>
          <div class="col-xs-6 col-md-4 ">
            <RepMember
              image="https://gov.texas.gov/uploads/images/general/governor.png"
              name="Gov. Greg Abbott"
              party="Republican"
              page="persona"
              twitter="GovAbbott"
              phone="(202) 224-5922"
            />
          </div>
        </div>

        <br></br>
        <h3>California</h3>
        <br></br>
          <div class="row">
          <div class="col-xs-6 col-md-4 ">
            <RepMember
              image="http://bioguide.congress.gov/bioguide/photo/F/F000062.jpg" 
              name="Sen. Dianne Feinstein"
              party="Democrat"
              page="personb"
              twitter="SenFeinstein"
              phone="(202) 224-3841"
            />
          </div>
          <div class="col-xs-6 col-md-4 ">
            <RepMember
              image="https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Senator_Harris_official_senate_portrait.jpg/220px-Senator_Harris_official_senate_portrait.jpg" 
              name="Sen. Kamala Harris"
              party="Democrat"
              page="personb"
              twitter="KamalaHarris"
              phone="(202) 224-3553"
            />
          </div>
          <div class="col-xs-6 col-md-4 ">
            <RepMember
              image="https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Gavin_Newsom_official_photo.jpg/220px-Gavin_Newsom_official_photo.jpg"
              name="Gov. Gavin Newsom"
              party="Democrat"
              page="personb"
              twitter="GavinNewsom"
              phone="(916) 445-2841"
            />
          </div>
        </div>

            <br></br>
        <h3>Florida</h3>
        <br></br>
          <div class="row">
          <div class="col-xs-6 col-md-4 ">
            <RepMember
              image="http://bioguide.congress.gov/bioguide/photo/R/R000595.jpg" 
              name="Sen. Marco Rubio"
              party="Republican"
              page="personc"
              twitter="marcorubio"
              phone="(202) 224-3041"
            />
          </div>
          <div class="col-xs-6 col-md-4 ">
            <RepMember
              image="https://www.rickscott.senate.gov/sites/default/files/glazed-cms-media/official_portrait_med.jpg" 
              name="Sen. Rick Scott"
              party="Republican"
              page="personc"
              twitter="SenRickScott"
              phone="(202) 224-5274"
            />
          </div>
          <div class="col-xs-6 col-md-4 ">
            <RepMember
              image="https://www.flgov.com/wp-content/uploads/desantis/Ron_DeSantis,_Official_Portrait,_113th_Congress.jpg"
              name="Gov. Ron DeSantis"
              party="Republican"
              page="personc"
              twitter="RonDeSantisFL"
              phone="(850) 488-7146"
            />
          </div>
        </div>

        <br></br>


      </div>

    );
  }

}

class Stats extends React.PureComponent {

  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text">The Statistics</h2>
        <div class="container">
          <table class="table table-cover">
            <thead>
              <tr>
                <th scope="col">Names</th>
                <th scope="col">Commits</th>
                <th scope="col">Issues</th>
                <th scope="col">Unit Tests</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    );
  }
}

class Purpose extends React.PureComponent {
  render() {
    return (
      <div class="container-fluid">
        <h2 class="title-text banner-text">Our Mission</h2>
        <p id="mission-statement"> 
          Our Mission is to help citizens be politically informed 
          and get excited to vote by providing them with unbiased 
          information on politicians and issues based on their location
        </p>
      </div>
    );
  }
}

class MainRep extends React.PureComponent {
  render() {
    return (
      <div id="main-container" class="container">
        <hr />
        <RepMemberList />
      </div>
    );
  }
}


ReactDOM.render(
  <MainRep />, 
  document.getElementById('react-representatives')
);
