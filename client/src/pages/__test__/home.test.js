import React from 'react';
import { shallow } from 'enzyme';

import MemberCard from '../../components/memberCard';
import About, { Purpose, MemberList } from '../about';

describe("Purpose", () => {
	it("renders with title and mission statement", () => {
		const wrapper = shallow(<Purpose />);
		expect(wrapper.find("h2").text()).toEqual("Our Mission");
		expect(wrapper.find("#mission-statement").exists()).toEqual(true);
	});
});

describe("Member List", () => {
	it("renders with 6 people in the group", () => {
		const wrapper = shallow(<MemberList />);
		const members = wrapper.find(MemberCard);
		expect(members.length).toEqual(6);
		expect(members.findWhere(member => member.props().name === "Jie Hao Liao").exists()).toEqual(true);
		expect(members.findWhere(member => member.props().name === "Jesus Vasquez").exists()).toEqual(true);
		expect(members.findWhere(member => member.props().name === "Jesse Martinez").exists()).toEqual(true);
		expect(members.findWhere(member => member.props().name === "Amiti Busgeeth").exists()).toEqual(true);
		expect(members.findWhere(member => member.props().name === "Lisa Barson").exists()).toEqual(true);
		expect(members.findWhere(member => member.props().name === "Fan Yang").exists()).toEqual(true);
	});
});

describe("About", () => {
	it("renders", () => {
		const wrapper = shallow(<About />);
	});
});
