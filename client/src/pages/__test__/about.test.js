import React from 'react';
import { shallow } from 'enzyme';

import Home, { Splash } from '../home';

describe("Splash", () => {
	it("renders with a title and an image", () => {
		const wrapper = shallow(<Splash />);
		expect(wrapper.find("video").exists()).toEqual(true);
		expect(wrapper.find("h1").exists()).toEqual(true);
		expect(wrapper.find("h1").at(0).text()).toEqual("VOTE WISELY");
	});
});

describe("Home", () => {
	it("renders with Splash", () => {
		const wrapper = shallow(<Home />);
		expect(wrapper.find(Splash).exists()).toEqual(true);
	});
});
