import React from 'react';
import { Link } from 'react-router-dom';
import { shallow } from 'enzyme';

import Map, { StateDialog } from '../map';

describe("State Dialog", () => {
	it("renders with correct given state and code information", () => {
		const wrapper = shallow(<StateDialog state="Texas" code="TX" fullScreen={false} senators={[]} reps={[]} />);
		expect(wrapper.find(Link).exists());
		expect(wrapper.find(Link).props().to.state).toEqual({
			state: "Texas",
			code: "TX"
		});
	});
});

describe("Map", () => {
	it("renders successfully", () => {
		const wrapper = shallow(<Map />);
	});
});
