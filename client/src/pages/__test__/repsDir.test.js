import GridListTile from '@material-ui/core/GridListTile';
import {shallow, mount} from 'enzyme';
import React from 'react';

import Banner from '../../components/banner';

import Politicians from '../politicians_table';

import MUIDataTable from 'mui-datatables';


describe("Representatives Directory", () => {

	it("successfully renders without crashing", () => {
		const wrapper = shallow(<Politicians />);
		expect(
			wrapper.length
		).toEqual(1);
	});

	it("renders representatives", () => {
		const wrapper = mount(<Politicians />);
		/* MUIDataTable moved into renderTable method */
		const wrapper2 = wrapper.find("#poli-table");
		expect(wrapper2.length).toEqual(2);
	});
});
