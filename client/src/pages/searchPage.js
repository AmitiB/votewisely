import React, {PureComponent} from 'react';
import {Container} from 'react-bootstrap';
import SearchResults from '../components/searchResults';
import SwipeableViews from 'react-swipeable-views';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import '../css/search.css';

export default class SearchPage extends PureComponent {

  state = {
    value: 0,
  };


  changeTab = (event, value) => {
    this.setState({value});
  };

  changeIndex = index => {
    this.setState({ value: index });
  };

  render() {
    return (
      <Container id="searchPage-container">
        {/* Create the Tab Bar and adds the headings */}
        <Tabs
            value={this.state.value}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            onChange={this.changeTab}
          >
            <Tab label="All" />
            <Tab label="Issues" />
            <Tab label="States" />
            <Tab label="Politicians" />
          </Tabs>

        {/* This Section used to make the tabs swipeable */}
        <SwipeableViews
          index={this.state.value}
          onChangeIndex={this.changeIndex}
        >
          <SearchResults type="all" query={this.props.match.params.result}/>
          <SearchResults type="issues" query={this.props.match.params.result}/>
          <SearchResults type="states" query={this.props.match.params.result}/>
          <SearchResults type="politicians" query={this.props.match.params.result}/>
        </SwipeableViews>
      </Container>
    );
  }
}
