import React, {PureComponent} from 'react';
import {Container} from 'react-bootstrap';
import Video from '../imgs/flag_waving.mp4';
import '../css/home.css';
/* GUI */

class Splash extends PureComponent {
  render() {
    return (
			<div>
        <h2 id="navID" className="glowing"><i className="fas fa-arrow-up"></i> Navigation</h2>
        <video id="background-video" loop autoPlay muted>
			    <source src={Video} type="video/mp4" />
			    <source src={Video} type="video/ogg" />
			    Your browser does not support the video tag.
				</video>
				<h1 id="splash-title">VOTE WISELY</h1>
				<h2 id="splash-subtitle">GET ALL THE INFORMATION</h2>
			</div>
    );
  }
}

export default class Home extends PureComponent {
	render () {
		return (
			<Container id="home-container">
				<Splash />
			</Container>
		);
	}
}

export { Splash };
