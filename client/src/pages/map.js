import React, {PureComponent} from 'react';
import { Link } from "react-router-dom";
import {RadioSVGMap, USA} from 'react-svg-map';
import 'react-svg-map/lib/index.css';
import '../css/map.css';

/* Material Dialog Imports */
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
// import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Slide from '@material-ui/core/Slide';
import Paper from '@material-ui/core/Paper';

/* Material UI GridList*/
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

import {nodelink} from '../api';


function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class StateDialog extends PureComponent {

  getImage(info) {
    if(info !== undefined)
      return info[4];
    else
      return "";
  }

  getName(info) {
    if(info !== undefined)
      return info[0];
    else
      return "";
  }

  getFirstName(info) {
    if(info !== undefined){
      return (String(info[0]).split(' '))[0]
    }
    else
      return "";
  }

  getLastName(info) {
    if(info !== undefined)
      return (String(info[0]).split(' '))[1]
    else
      return "";
  }

  render() {

    const { fullScreen } = this.props;

    return (
      <Dialog
          fullScreen={fullScreen}
          open={this.props.open}
          onClose={this.props.onClose}
          aria-labelledby="responsive-dialog-title"
          fullWidth={true}
          maxWidth='sm'
          TransitionComponent={Transition}
          keepMounted
        >
        <DialogTitle id="responsive-dialog-title" disableTypography={true}><h1>{this.props.state}</h1></DialogTitle>
          <DialogContent>
              <h3 className="ModalSectionTitle">Senators<br /></h3>

              <GridList cellHeight={150} cols={4}>
                <GridListTile>
                  <img src={this.getImage(this.props.senators[0])} alt=""/>
                  <GridListTileBar
                    title={<h5 className="title">{this.getFirstName(this.props.senators[0])}</h5>}
                    subtitle={<span className="title">{this.getLastName(this.props.senators[0])}</span>}
                    actionIcon={
                      <IconButton
                        color="primary"
                        href={"/politicians/" + String(this.getName(this.props.senators[0])).replace(" ", "-").toLowerCase()}>
                        <InfoIcon />
                      </IconButton>
                    }
                  />
                </GridListTile>
                <GridListTile>
                  <img src={this.getImage(this.props.senators[1])} alt=""/>
                  <GridListTileBar
                    title={<h5 className="title">{this.getFirstName(this.props.senators[1])}</h5>}
                    subtitle={<span className="title">{this.getLastName(this.props.senators[1])}</span>}
                    actionIcon={
                      <IconButton
                        color="primary"
                        href={"/politicians/" + String(this.getName(this.props.senators[1])).replace(" ", "-").toLowerCase()}>
                        <InfoIcon />
                      </IconButton>
                    }
                  />
                </GridListTile>
              </GridList>


              <h3 className="ModalSectionTitle">Represenatives<br /></h3>

              <GridList cellHeight={150} cols={4}>
              {this.props.reps.slice(0, 4).map(tile => (
    						<GridListTile key={tile[4]}>
    							<img src={tile[4]} alt={tile[0]}/>
    							<GridListTileBar
                  title={<h5 className="title">{this.getFirstName(tile)}</h5>}
                  subtitle={<span className="title">{this.getLastName(tile)}</span>}
    								actionIcon={
    									<IconButton
                        color="primary"
                        href={"/politicians/" + String(this.getName(tile)).replace(" ", "-").toLowerCase()}>
    										<InfoIcon />
    									</IconButton>
    								}
    							/>
    						</GridListTile>
    					))}
              </GridList>
              <h3 className="ModalSectionTitle">More Info<br /></h3>
               <Link to={{
                 pathname: StateLink,
                 state: {
                   state: this.props.state,
                   code: this.props.code
                 }
               }}><h4>{this.props.state}</h4></Link>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.onClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
    );
  }
}

StateDialog.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
};

var ResponsiveStateDialog = withMobileDialog()(StateDialog);

class MapKey extends PureComponent {
  render () {
    return (
      <div>
        <Paper id="mapkey" elevation={4}>
          <h4>2016 Presidential Election</h4>
          <svg width="300" height="150">

            <rect fill="red" x="10%" y="10" rx="0" ry="0" width="60" height="20" className="mapkey-item"/>
              <text x="35%" y="15" alignmentBaseline="hanging">Republican win over 5%</text>
            <rect fill="#fa8072" x="10%" y="40" rx="0" ry="0" width="60" height="20" className="mapkey-item"/>
              <text x="35%" y="45" alignmentBaseline="hanging">Republican win under 5% </text>
            <rect fill="blue" x="10%" y="70" rx="0" ry="0" width="60" height="20" className="mapkey-item"/>
              <text x="35%" y="75" alignmentBaseline="hanging">Democratic win over 5% </text>
            <rect fill="#3895d3" x="10%" y="100" rx="0" ry="0" width="60" height="20" className="mapkey-item"/>
              <text x="35%" y="105" alignmentBaseline="hanging">Democratic win under 5% </text>

          </svg>
        </Paper>
      </div>
    );
  }
}

var StateName = "";
var StateCode = "";
var StateLink = "";

export default class Map extends PureComponent {

	constructor(props) {
		super(props);
		this.state = {
      selectedState: "none",
      modalShow: false,
      sens: [],
      reps: [],
      sensData: [],
      repsData: [],
      fullMap: "",
      fullTitle: "",
      fullButton: false
    };
	}

	toggleStateFunction (e) {
		StateName = e.getAttribute('name');
		StateCode = e.getAttribute('id');
		StateLink = "/states/" + StateName.replace(' ', '-').toLowerCase();
		this.setState({selectedState:StateName});
		this.setState({modalShow: true});
    this.getStateData(StateName);
	}

  getStateData(stateName) {
    console.log(StateCode);
		fetch(nodelink + "/api/reps/senate/?state=" + StateCode.toUpperCase())
			.then(res => res.json())
			.then(res => res.response)
			.then(rep => this.setState({sens: rep}))
      .then(res => this.updateData(this.state.sens, "Senate"));

		fetch(nodelink + "/api/reps/house/?state=" + StateCode.toUpperCase())
			.then(res => res.json())
			.then(res => res.response)
			.then(rep => this.setState({reps: rep}))
      .then(res => this.updateData(this.state.reps, "House"));

  }

  updateData(pulledData, wing){
    var extData = [];
		for(var i = 0; i < pulledData.length; i++){
			var tempData = []
			var Name = this.getName(pulledData, i);
			var Title = this.getTitle(pulledData, i);
			var Facebook = this.getFacebook(pulledData, i);
			var Twitter = this.getTwitter(pulledData, i);
			var Image = this.getImage(pulledData, i);
			tempData[0] = Name;
			tempData[1] = Title;
			tempData[2] = Facebook;
			tempData[3] = Twitter;
			tempData[4] = Image;
			extData[i] = tempData;
		}
		if(wing === "Senate"){
			this.setState({sensData : extData});
    }
		else if(wing === "House")
			this.setState({repsData : extData});
		else
			console.log("Wing = " + wing);
  }

  getTitle(pulledData, i){
    if(pulledData.length > 0)
      return pulledData[i].title;
  }

  getName(pulledData, i){
    if(pulledData.length > 0)
      return pulledData[i].first_name + " " + pulledData[i].last_name;
  }

  getFacebook(pulledData, i){
    if(pulledData.length > 0)
      return pulledData[i].facebook_account;
  }

  getTwitter(pulledData, i){
    if(pulledData.length > 0)
      return pulledData[i].twitter_account;
  }

  getBirth(pulledData, i){
    if(pulledData.length > 0)
      return pulledData[i].date_of_birth;
  }

  getImage(pulledData, i){
    if(pulledData.length > 0)
      return pulledData[i].img_url;
  }

  /* Change the map between fullscreen and fullWidth */
  toggleFullscreen = e => event => {
    this.setState({ fullButton: event.target.checked });
    if(!this.state.fullButton){
      this.setState({fullTitle: "fullTitle"});
      this.setState({fullMap: "fullMap"});
    } else {
      this.setState({fullTitle: ""});
      this.setState({fullMap: ""});
    }
  }

	render () {

		let modalClose = () => this.setState({ modalShow: false });

		return (
			<div id="map-container">
        {/* <img src={BackgroundImage} id="background-usa" alt="BackgroundImage"/> */}
				<h1 className='fullTitle' id="map-title">Interactive Map</h1>
				<RadioSVGMap
          className="svg-map fullMap"
          map={USA} id="usa-map"
          onChange={(e) => this.toggleStateFunction(e)}/>

        {/* <Paper id="switch-container" elevation={4}>
          <h4>Toggle Fullscreen</h4>
          <Switch
            checked={this.state.fullButton}
            onChange={this.toggleFullscreen()}
            value="toggleMap"
            color="primary"
          />
        </Paper> */}

				<ResponsiveStateDialog
          open={this.state.modalShow}
          onClose={modalClose}
					state={StateName}
					link={StateLink}
          code={StateCode}
          senators={this.state.sensData}
          reps={this.state.repsData}
        />
        <MapKey />

			</div>
		);
	}
}

export { StateDialog };
