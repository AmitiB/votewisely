import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from "mui-datatables";
import Container from 'react-bootstrap/Container';
import HiliteCell from '../components/hiliteCell';
import SearchBar from '../components/searchBar';
import SearchResults from '../components/searchResults';
import { nodelink } from '../api';
import '../css/statesDir.css';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

export default class StatesDir extends PureComponent {

	constructor(props){
		super(props);
		this.state = {
			ids: [],
			states: [],
			data: [],
			dataOrg: [],
			search: "?",
			loaded: false,
			updated: false,
			repList: [],
			repsLoaded: false,
			stateReps: {},
			filterList: [],
			query: '',
			value: 0
		}
	}

	columns = [
		{
			name: "Abbrev.",
			options: {
				filter: false,
				customBodyRender: (value, tableMeta, updateValue) => {
					return (
						<HiliteCell
							className="hover-cursor"
						 	value={value}
							search={tableMeta.tableState.searchText}
						/>
					);
				},
			}
		},
		{
			name: "Flag",
			options: {
				filter: false,
				sort: false,
				customBodyRender: (value, tableMeta, updateValue) => {
					return (
						<img
							className="hover-cursor"
							src={value}
							style={{width:'auto', height:'40px', marginTop:'10px', marginBottom:'10px'}}
							alt=""
						/>
					)
				}
			}
		},
		{
			name: "State",
			options: {
				filter: false,
				customBodyRender: (value, tableMeta, updateValue) => {
					return (
						<HiliteCell
							className="hover-cursor"
						 	value={value}
							search={tableMeta.tableState.searchText}
						/>
					);
				},
			}
		},
		{
			name: "Population",
			options: {
				filter: false,
				customBodyRender: (value, tableMeta, updateValue) => {
					return (
						<HiliteCell
							className="hover-cursor"
						 	value={value}
							search={tableMeta.tableState.searchText}
							number={true}
						/>
					);
				},
			}
		},
		{
			name: "Legislature",
			options: {
				filter: false,
				customBodyRender: (value, tableMeta, updateValue) => {
					return (
						<HiliteCell
							className="hover-cursor"
						 	value={value}
							search={tableMeta.tableState.searchText}
						/>
					);
				},
			}
		},
		{
			name: "Population Range",
			options: {
				display: false,
				filter: true
			}
		},
		{
			name: "Number of Representatives",
			options: {
				display: true,
				filter: true
			}
		},
		{
			name: "Party (2016)",
			options: {
				filter: true,
				customBodyRender: (value, tableMeta, updateValue) => {
					return (
						<HiliteCell
							className="hover-cursor"
						 	value={value}
							search={tableMeta.tableState.searchText}
						/>
					);
				},
			}
		}
	]

	static contextTypes = {
		router: PropTypes.object
	}

	options = {
	  filterType: 'checkbox',
		pageSizeOptions: [10, 15, 20],
		print: false,
		download: false,
		selectableRows: false,
		onRowClick: (data, meta)=>this.renderRedirect(data, meta),
	}

	optionsMobile = {
	  filterType: 'checkbox',
		pageSizeOptions: [10, 15, 20],
		print: false,
		download: false,
		responsive: 'scroll',
		selectableRows: false,
		onRowClick: (data, meta)=>this.renderRedirect(data, meta),
	}

	renderRedirect(data, meta) {
		var state_name = String(this.state.states[meta.dataIndex].name).toLowerCase()
											.replace(" ", '-');
		this.context.router.history.push("/states/"+state_name);

	}

	componentDidMount() {
		fetch(nodelink + "/api/states/")
			.then(res => res.json())
			.then(res => res.response)
			.then(rep => this.setState({states: rep}))
			.then(rep =>
				fetch(nodelink + "/api/reps/house")
					.then(r => r.json())
			 		.then(r => {
			       this.setState({ repList: r.response });
			       this.setState({ repsLoaded: true});
						 this.getNumberOfReps();
			   })
			)
			.then(rep => this.setData());

	}

	getAbbrev(i) {
			return this.state.states[i].abbrev;
	}

	getName(i) {
			return this.state.states[i].name;
	}

	getPopulation(i) {
			return this.state.states[i].population;
	}

	getLegislature(i) {
			return this.state.states[i].legislature_name;
	}

	getNumberOfReps(name) {
		var dict = {};
		if (this.state.repsLoaded) {
			for (var i = 0; i < this.state.repList.length; i++) {
			   if (this.state.repList[i].state_fullname in dict) {
					 dict[this.state.repList[i].state_fullname] += 1;
				 } else {
					 dict[this.state.repList[i].state_fullname] = 1;
				 }
			}
			this.setState({stateReps : dict});
		}
	}

	getPopulationRange(population){
		switch(true){
			case (population < 1000000):{
				return "0-1000000";
			}
			case (population < 2000000):{
				return "1000000-2000000";
			}
			case (population < 3000000):{
				return "2000000-3000000";
			}
			case (population < 4000000):{
				return "3000000-4000000";
			}
			case (population < 5000000):{
				return "4000000-5000000";
			}
			case (population < 6000000):{
				return "5000000-6000000";
			}
			case (population < 7000000):{
				return "6000000-7000000";
			}
			case (population < 8000000):{
				return "7000000-8000000";
			}
			case (population < 9000000):{
				return "8000000-9000000";
			}
			case (population < 10000000):{
				return "9000000-1000000";
			}
			case (population < 15000000):{
				return "1000000-1500000";
			}
			case (population > 15000000):{
				return "Over 1500000";
			}
			default:
				return "invalid population";
		}
	}

	getParty(party){
		if(party === 'R')
			return "Republican"
		else if(party === 'D')
			return "Democrat"
	}

	setData() {
			var tempData = [];
			for(var i = 0; i < this.state.states.length; i++){
				var dataSlot=[];
				dataSlot[0] = this.getAbbrev(i);
				dataSlot[1] = this.state.states[i].banner_url;
				dataSlot[2] = this.getName(i);
				dataSlot[3] = this.getPopulation(i);
				dataSlot[4] = this.getLegislature(i);
				dataSlot[5] = this.getPopulationRange(this.getPopulation(i));
				dataSlot[6] = this.state.stateReps[this.state.states[i].name];
				dataSlot[7] = this.getParty(this.state.states[i].party);
				tempData[i] = dataSlot;
			}
			this.setState({data: tempData});
			this.setState({dataOrg: tempData});
			this.setState({loaded: true});
	}

	renderTable(){
		if(this.state.loaded){
			return(
				<div>
					<MUIDataTable
						title=<div className="table-dir-title">States Directory</div>
					  data={this.state.data}
					  columns={this.columns}
					  options={this.options}
						className="table-desk"
					/>

					<MUIDataTable
						title=<div className="table-dir-title">States Directory</div>
					  data={this.state.data}
					  columns={this.columns}
					  options={this.optionsMobile}
						className="table-mobile"
					/>
				</div>
			);
		}
		else
			return(
				<h2 id="sd-title">LOADING TABLE...</h2>
			);
	}

	renderSearch(query){
		if(query !== ""){
			return(
				<div style={{background:'white'}}>
					<div className="table-dir-title"
						style={{textAlign: 'left', padding: '2px 0 0 24px', fontSize: '2.5em'}}>
							Search
					</div>
					<SearchBar
						placeholder="Search States' Pages"
						className='search-bar sb-directory'
						searchFunction={(search) => {
							this.setState({query: search})
						}}
					/>
					<SearchResults type="states" query={query}/>
				</div>
			);
		}
		else
		return(
			<div style={{background: 'white', paddingBottom: '50px'}}>
				<div className="table-dir-title"
					style={{textAlign: 'left', padding: '2px 0 0 24px', fontSize: '2.5em'}}>
						Search
				</div>
				<SearchBar
					placeholder="Search States' Pages"
					className='search-bar sb-directory'
					searchFunction={(search) => {
						this.setState({query: search})
					}}
				/>
			</div>
		);
	}

	changeTab = (event, value) => {
    this.setState({value});
  }

	render () {

		return (
			<Container id="statesdir-container">
				<img src="https://elevenews.com/wp-content/uploads/2018/08/us-government.jpg" alt="" id="statesdir-bg" />
				<Container id="state-table">
					<Tabs
	            value={this.state.value}
	            indicatorColor="primary"
	            textColor="primary"
	            variant="fullWidth"
	            onChange={this.changeTab}
							style={{background:'white'}}
	          >
	            <Tab label="Directory"/>
	            <Tab label="Search"/>
	          </Tabs>
						{this.state.value === 0 &&
							<div>
								{this.renderTable()}
							</div>}
		        {this.state.value === 1 &&
							<div>
								{this.renderSearch(this.state.query)}
							</div>}
				</Container>

			</Container>
		);
	}
}
