import React, { PureComponent } from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Banner from '../components/banner'
import { nodelink } from '../api';
import Checkmark from '../imgs/checkmark.png';
import '../css/issue.css';

function getStateFromCode(name) {
  var stateName = "";
  name = String(name).toLowerCase();
  switch(name) {
    case 'ak':
      stateName = 'Alaska';
      break;
    case 'al':
      stateName = 'Alabama';
      break;
    case 'az':
      stateName = 'Arizona';
      break;
    case 'ar':
      stateName = 'Arkansas';
      break;
    case 'ca':
      stateName = 'California';
      break;
    case 'co':
      stateName = 'Colorado';
      break;
    case 'ct':
      stateName = 'Connecticut';
      break;
    case 'de':
      stateName = 'Delaware';
      break;
    case 'fl':
      stateName = 'Florida';
      break;
    case 'ga':
      stateName = 'Georgia';
      break;
    case 'hi':
      stateName = 'Hawaii';
      break;
    case 'id':
      stateName = 'Idaho';
      break;
    case 'il':
      stateName = 'Illinois';
      break;
    case 'in':
      stateName = 'Indiana';
      break;
    case 'ia':
      stateName = 'Iowa';
      break;
    case 'ks':
      stateName = 'Kansas';
      break;
    case 'ky':
      stateName = 'Kentucky';
      break;
    case 'la':
      stateName = 'Louisiana';
      break;
    case 'me':
      stateName = 'Maine';
      break;
    case 'md':
      stateName = 'Maryland';
      break;
    case 'ma':
      stateName = 'Massachusetts';
      break;
    case 'mi':
      stateName = 'Michigan';
      break;
    case 'mn':
      stateName = 'Minnesota';
      break;
    case 'ms':
      stateName = 'Mississippi';
      break;
    case 'mo':
      stateName = 'Missouri';
      break;
    case 'mt':
      stateName = 'Montana';
      break;
    case 'ne':
      stateName = 'Nebraska';
      break;
    case 'nv':
      stateName = 'Nevada';
      break;
    case 'nh':
      stateName = 'New Hampshire';
      break;
    case 'nj':
      stateName = 'New Jersey';
      break;
    case 'nm':
      stateName = 'New Mexico';
      break;
    case 'ny':
      stateName = 'New York';
      break;
    case 'nc':
      stateName = 'North Carolina';
      break;
    case 'nd':
      stateName = 'North Dakota';
      break;
    case 'oh':
      stateName = 'Ohio';
      break;
    case 'ok':
      stateName = 'Oklahoma';
      break;
    case 'or':
      stateName = 'Oregon';
      break;
    case 'pa':
      stateName = 'Pennsylvania';
      break;
    case 'ri':
      stateName = 'Rhode Island';
      break;
    case 'sc':
      stateName = 'South Carolina';
      break;
    case 'sd':
      stateName = 'South Dakota';
      break;
    case 'tn':
      stateName = 'Tennessee';
      break;
    case 'tx':
      stateName = 'Texas';
      break;
    case 'ut':
      stateName = 'Utah';
      break;
    case 'vt':
      stateName = 'Vermont';
      break;
    case 'va':
      stateName = 'Virginia';
      break;
    case 'wa':
      stateName = 'Washington';
      break;
    case 'wv':
      stateName = 'West Virginia';
      break;
    case 'wi':
      stateName = 'Wisconsin';
      break;
    case 'wy':
      stateName = 'Wyoming';
      break;
    default:
      stateName = '';
  }
  return stateName.toLowerCase().replace(' ', '-');
}

function getRepLink(name) {
  return name.toLowerCase().replace(' ', '-');
}


export default class Issue extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      issueData: {},
      vid1: null,
      vid2: null
    };
  }

  componentDidMount() {
    fetch(nodelink + "/api/issues2/" + this.props.match.params.issue)
      .then(res => res.json())
      .then(res => res.response)
      .then(issue => {
        if (issue) {
          this.setState({ issueData: issue });
          this.attachBill(issue.bill_1);
          this.attachBill(issue.bill_2);
          this.attachBill(issue.bill_3);
          this.attachBill(issue.bill_4);
          this.attachBill(issue.bill_5);
          this.attachStatement(issue.statement_1);
          this.attachStatement(issue.statement_2);
          this.attachStatement(issue.statement_3);
          this.attachStatement(issue.statement_4);
          this.attachStatement(issue.statement_5);
          this.setState({vid1: issue.youtube_id1});
          this.setState({vid2: issue.youtube_id2});
        } else {
          /* 404 case */
        }
      });
  }

  componentDidUpdate() {
    window.scrollTo(0, 0);
  }

  attachBill(bill_id) {
    if (bill_id) {
      fetch(nodelink + "/api/bills2?bill_id=\"" + bill_id + "\"")
        .then(res => res.json())
        .then(res => res.response[0])
        .then(bill => {
          if (this.state.bills) {
            this.setState({ bills: this.state.bills.concat([bill]) });
          } else {
            const bills = [bill];
            this.setState({ bills: bills });
          }
        });
    }
  }

  attachStatement(statement_id) {
    if (statement_id) {
      fetch(nodelink + "/api/statements?id=\"" + statement_id + "\"")
        .then(res => res.json())
        .then(res => res.response[0])
        .then(statement => {
          if (this.state.statements) {
            this.setState({ statements: this.state.statements.concat([statement]) });
          } else {
            const statements = [statement];
            this.setState({ statements: statements });
          }
        });
    }
  }

  renderParagraphs(text, hash) {
    if (text) {
      var paragraphs = text.split("\n");
      return (
        <div>
          {paragraphs.map((paragraph, idx) => (
            <p key={hash + idx}>
              {paragraph}
            </p>
          ))}
        </div>
      );
    }
    return;
  }

  renderDescription() {
    if (this.state.issueData.summary) {
      return (
        <div id="description">
          <h2>Description</h2>
          <hr />
          <div className="description-container">
            <div className="description-paragraph">
              {this.renderParagraphs(this.state.issueData.summary, "d")}
              <p className="credits" id="issue-links">
                Credits to&nbsp;
                <a href={this.state.issueData.src} target="_blank" rel="noopener noreferrer" className="link">
                  iSideWith.
                </a>
              </p>
            </div>
            <div className="description-image">
              <img
                alt={this.state.issueData.img1}
                src={process.env.PUBLIC_URL + this.state.issueData.img1}
                id="first-image" />
              </div>
            </div>
        </div>
      );
    }
    return (
      <div>
        <h2>Description</h2>
        <hr />
        <p>Loading...</p>
      </div>
    );
  }

  renderPoll(side, hash) {
    if (side) {
      return (
        <div>
          {side.map((obj, idx) => (
            <p key={hash + idx}>
              {obj.perc}% {obj.comment}
            </p>
          ))}
        </div>
      );
    }
  }

  percentageSum(side) {
    var sum = 0;
    for (var i = 0; i < side.length; i++) {
      sum += side[i].perc;
    }
    return sum;
  }

  renderQuestion() {
    if (this.state.issueData.question) {
      return (
        <div id="question">
          <h2>Question</h2>
          <hr />
          <h4>{this.renderParagraphs(this.state.issueData.question, "p")}</h4>
          <div className="pie-chart">
            <div className="pie-percent">
              <p>{this.state.issueData.side_yes[0].comment}</p>
              <p>{this.percentageSum(this.state.issueData.side_yes)}%</p>
            </div>
            <img
              alt={this.state.issueData.pie}
              src={process.env.PUBLIC_URL + this.state.issueData.pie}
              id="second-image" />
            <div className="pie-percent">
                <p>{this.state.issueData.side_no[0].comment}</p>
                <p>{this.percentageSum(this.state.issueData.side_no)}%</p>
            </div>
          </div>
          <div>
            <p>Other responses:</p>
          </div>
          <div className="question">
            <div className="poll-yes">{this.renderPoll(this.state.issueData.side_yes.slice(1), "c")}</div>
            <div className="poll-middle">{this.renderPoll(this.state.issueData.side_center, "c")}</div>
            <div className="poll-no">{this.renderPoll(this.state.issueData.side_no.slice(1), "c")}</div>
          </div>
          <div>
            <p className="credits" id="issue-links">
              Credits to&nbsp;
              <a href={this.state.issueData.src} target="_blank" rel="noopener noreferrer" className="link">
                iSideWith.
              </a>
            </p>
          </div>
        </div>
      );
    }
    return (
      <div>
        <h2>Question</h2>
        <hr />
        <p>Loading...</p>
      </div>
    );
  }

  renderCategory() {
    if (this.state.issueData.category) {
      return (
        <div id="category">
          <img
            alt={this.state.issueData.img2}
            src={process.env.PUBLIC_URL + this.state.issueData.img2}
            id="category-image" />
          <p>Category: {this.state.issueData.category}</p>
        </div>
      );
    }
  }

  renderStatements() {
    if (this.state.statements) {
      return (
        <div id="statements">
          <h2 className="recent-congressional-title">Recent Congressional Statements</h2>
          <hr />
          {this.state.statements.map((statement, idx) => {
            return (
              <div key={"statements" + idx} id="issue-links">
                <p>
                  <a href={statement.url} target="_blank" rel="noopener noreferrer" className="link">
                    {statement.title}
                  </a>
                  &nbsp;by <Link to={"/politicians/" + getRepLink(statement.name)}> {statement.name} </Link>
                  on {statement.date} in&nbsp;
                  <Link to={"/states/" + getStateFromCode(statement.state)}>
                    {statement.state}.
                  </Link>
                </p>
              </div>
            );
          })}
          <p id="issue-links">
            <Link to="/politicians">Find more info about the representatives that wrote these statements</Link>
          </p>
          <p className="credits" id="issue-links">
            Credits to&nbsp;
            <a href="https://www.congress.gov/" target="_blank" rel="noopener noreferrer" className="link">
              Congress.gov
            </a>
            &nbsp;and&nbsp;
            <a
              href="https://projects.propublica.org/api-docs/congress-api/"
              target="_blank" rel="noopener noreferrer" className="link">
              ProPublica API
            </a>
          </p>
        </div>
      );
    }
    return (
      <div>
        <h2 className="recent-congressional-title">Recent Congressional Statements</h2>
        <hr />
        <p>None</p>
      </div>
    );
  }

  renderBills() {
    if (this.state.bills) {
      return (
        <div id="bills">
          <h2>Recent Congressional Bills</h2>
          <hr />
          {this.state.bills.map((bill, idx) => {
            return (
              <div key={"bills" + idx} className="bills" id="issue-links">
                <div>
                  <img
                    alt={this.state.issueData.img1}
                    src={Checkmark}
                    id="checkmark" />
                </div>
                <div>
                  <p>
                    <a href={bill.congressdotgov_url} target="_blank" rel="noopener noreferrer" className="link">
                      {bill.title}
                    </a>
                    &nbsp;By <Link to={"/politicians/" + getRepLink(bill.sponsor_name)}> {bill.sponsor_name} </Link>
                    ({bill.sponsor_party === "D" ? "Democrat" : "Republican"}) from&nbsp;
                    <Link to={"/states/" + getStateFromCode(bill.sponsor_state)}>
                      {bill.sponsor_state}.
                    </Link>
                  </p>
                </div>
              </div>
            );
          })}
          <p id="issue-links">
            <Link to="/map">Find more info about how different states view these bills</Link>
          </p>
          <p className="credits" id="issue-links">
            Credits to&nbsp;
            <a href="https://www.congress.gov/" target="_blank" rel="noopener noreferrer" className="link">
              Congress.gov
            </a>
            &nbsp;and&nbsp;
            <a
              href="https://projects.propublica.org/api-docs/congress-api/"
              target="_blank" rel="noopener noreferrer" class="link">
              ProPublica API
            </a>
          </p>
        </div>
      );
    }
    return (
      <div>
        <h2 className="recent-congressional-title">Recent Congressional Bills</h2>
        <hr />
        <p>None</p>
      </div>
    );
  }

  addVideo(video_id){
    if(video_id)
    return(
      <iframe
      style={{
        width: '100%',
        minHeight: '100%',
        position: 'absolute',
        padding: '20px'
      }}
      src={`https://www.youtube.com/embed/${video_id}`}
      frameBorder="0"
      allowFullScreen
    />
    );
  }

  renderVideos(){
    return(
      <Container id="bills">
        <br />
        <h2>Relevant Videos</h2>
        <hr />
        <Row>
          <Col md={6} sm={12} style={{height: '350px'}}>
            {this.addVideo(this.state.vid1)}
          </Col>
          <Col md={6} sm={12} style={{height: '350px'}}>
            {this.addVideo(this.state.vid2)}
          </Col>
        </Row>
    </Container>
    );
  }

  render() {
    return (
      <div>
        <Banner
          text = {this.state.issueData.name}
          img = {this.state.issueData.banner}
        />
        <div className="container banner-gap">
          <div className="body-sections">
            <div className="left-body">
              {this.renderDescription()}
              {this.renderQuestion()}
            </div>
            <div className="right-body">
              {this.renderCategory()}
              {this.renderStatements()}
            </div>
          </div>
          {this.renderBills()}
          {this.renderVideos()}
        </div>
      </div>
    );
  }
}
