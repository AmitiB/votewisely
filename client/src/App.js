import React, { Component } from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';

import AppBar from './components/appBar';
import NavBar from './components/navbar';
import Banner from './components/banner';
import './App.css';

/* import our pages */
import About from './pages/about';
import SearchPage from './pages/searchPage';
import Home from './pages/home';
import Issue from './pages/issue';
import Issues from './pages/issues';
import Map from './pages/map';
import States from './pages/states';
import StatesDir from './pages/statesDir';
import Reps from '../src/pages/representatives';
import RepsDir from '../src/pages/politicians_table';
import Visual from '../src/pages/visualization';
/* Banner Image imports */
import AboutBanner from './imgs/about_banner.jpg';
import IssuesBanner from './imgs/issues.jpeg';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
        <div>
        <NavBar className="NavBar"/>
        <AppBar />
          <Route exact className="Home" path='/' component={Home}/>
          <Route className="Search" path='/search/:result' component={SearchPage}/>
          <Route exact={true} path='/about' render={() => (
            <div className="About banner-gap">
              <Banner
                img={AboutBanner}
                text="ABOUT OUR PROJECT"
              />
              <About />
            </div>
          )}/>
          <Route exact={true} path='/issues' render={() => (
            <div className="Issues banner-gap">
              <Banner
                img={IssuesBanner}
                text="THE ISSUES"
              />
              <Issues />
            </div>
          )}/>
          <Route exact={true} path='/issues/:issue' component={Issue} />
          <Route exact={true} path='/politicians' render={() => (
            <div className="Reps">
              <RepsDir />
            </div>
          )}/>
          <Route exact={true} path='/politicians/:reps' component={Reps} />
          <Route exact={true} path='/map' render={() => (
            <div className="Map">
              <Map />
            </div>
          )}/>
          <Route exact={true} path='/states' component={StatesDir} />
          <Route exact={true} path='/states/:states' render={(props) => (
              <States {...props}/>
          )}/>
          <Route exact={true} path='/visualization' component={Visual} />
        </div>
      </Router>
      </div>
    );
  }
}

export default App;
