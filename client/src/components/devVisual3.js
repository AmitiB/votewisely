import React, {PureComponent} from 'react';
import * as d3 from "d3";
import '../css/devVisualPie.css';
export default class DevVisual3 extends PureComponent{


  constructor(props) {
    super(props);
    this.state = {
      width : 800,
      height : 500,
      bills: [],
      isLoaded: false,
      formatted_bills_2: [],
      issue_names: [],
      issue_numbers: [],
      issues_dict: []
    };
  }
  drawPie(poop){
    var svg = d3.select("#mysvg3")
             .attr("width", 960)
             .attr("height", 500)
             .append("g");

    svg.append("g").attr("class", "slices");
    svg.append("g").attr("class", "labels");
    svg.append("g").attr("class", "lines");
    var width = 960;
    var height = 500;
    var radius = Math.min(width, height)/2;
    var color = d3.scaleOrdinal(d3.schemeCategory10);
    var data_2 = this.state.formatted_bills_2;

    var pie_2 = d3.pie().sort(null).value(d => d.num);
    var arc = d3.arc().innerRadius(radius*0.8).outerRadius(radius*0.6);

    var outerArc = d3.arc()
                .outerRadius(radius * 0.9)
                .innerRadius(radius * 0.9);
    svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
    svg.selectAll('path')
        .data(pie_2(data_2))
        .enter()
        .append('path')
        .attr('d', arc)
        .attr('fill', (d,i)=> color(i));
    svg.append('g').classed('labels',true);
    svg.append('g').classed('lines',true);


    svg.select('.lines')
                .selectAll('polyline')
                .data(pie_2(data_2))
                .enter().append('polyline')
                .attr('points', function(d) {
                    var pos = outerArc.centroid(d);
                    pos[0] = radius * 0.95 * (midAngle(d) < Math.PI ? 1 : -1);
                    return [arc.centroid(d), outerArc.centroid(d), pos]
                });

    var label = svg.select('.labels').selectAll('text')
                .data(pie_2(data_2))
                .enter().append('text')
                .attr('dy', '.35em')
                .html(function(d) {
                    return d.data.name;
                })
                .attr('transform', function(d) {
                    var pos = outerArc.centroid(d);
                    pos[0] = radius * 0.95 * (midAngle(d) < Math.PI ? 1 : -1);
                    return 'translate(' + pos + ')';
                })
                .style('text-anchor', function(d) {
                    return (midAngle(d)) < Math.PI ? 'start' : 'end';
                });

    svg.append('text')
                .attr('class', 'toolCircle')
                .attr('dy', -15) // hard-coded. can adjust this to adjust text vertical alignment in tooltip
                .html('Number of Bills Per Issue') // add text to the circle.
                .style('font-size', '.9em')
                .style('text-anchor', 'middle');

    function midAngle(d) { return d.startAngle + (d.endAngle - d.startAngle) / 2; }
    return svg.node();
  }


  sortBills2(data){
    var formatted_bills_2 = [];
    var the_dict = {};
    var issue_list = []

    for(var i = 0; i < data.length; i ++){
      var bill = data[i];
      var issue_name = bill.connections[0];
      if(!issue_list.includes(issue_name)){
        issue_list.push(issue_name);
      }
    }

    for(var l = 0; l < issue_list.length; l++){
      var temp_dict = {};
      temp_dict["name"] = issue_list[l];
      temp_dict["num"] = 0;
      the_dict[issue_list[l]] = 0;
      formatted_bills_2.push(temp_dict)
    }

    for(var x = 0; x < data.length; x ++){
      bill = data[x];
      issue_name = bill.connections[0];
      the_dict[issue_name] +=1;

      for(var b = 0; b < formatted_bills_2.length; b++){
        if(formatted_bills_2[b].name === issue_name){
          formatted_bills_2[b].num +=1;
        }
      }
    }

    for(var t = 0; t < formatted_bills_2.length; t++){
        if(formatted_bills_2[t].name === "Gender Marker Updates On Identification Documents"){
             var temp_dict_replace = {};
             temp_dict_replace["name"] = "Gender I.D. Documents"
             temp_dict_replace["num"] = formatted_bills_2[t].num;
             formatted_bills_2[t] = temp_dict_replace;
        break;
      }
    }
    this.setState({formatted_bills_2: formatted_bills_2});
    this.setState({issue_names: issue_list});
    this.setState({issues_dict: the_dict});
  }



  componentDidMount() {
      fetch("https://api.rainbowconnection.me/v1/bills/all")
      .then(res => res.json())
      .then(res => this.setState({bills: res}))
      .then(res => this.sortBills2(this.state.bills))
      .then(res => this.drawPie(this.state.formatted_bills_2));
  }


  render(){
    return (
      <div style={{background: "whitesmoke", paddingBottom: '40px', marginTop: '50px'}}>
            <br></br>
            <h5>Bills per Issue</h5>
            <p>Compares the number of bills per issue for RainbowConnection</p>
           <svg id="mysvg3" width={this.state.width} height = {this.state.height}> </svg>
      </div>
    );
  }
}
