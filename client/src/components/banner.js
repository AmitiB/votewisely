import React, {Component} from 'react';
import '../css/banner.css'

export default class Banner extends Component {
	render() {
		if (this.props.img) {
			return (
				<div id="banner-container">
					<img src={this.props.img} id="banner-image" alt="Loading..."/>
					<div>
						<h1 id="banner-text">
							{this.props.text}
						</h1>
					</div>
				</div>
			);
		}
		return (
			<div id="banner-container">
				<h1 id="banner-text" style={{ color: 'black' }}>
					Loading...
				</h1>
			</div>
		);
	}
}
