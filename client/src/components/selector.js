import React, { PureComponent } from 'react';

export default class Selector extends PureComponent {
  constructor(props) {
    /* Props args: list, onClick, defaultIndex (optional) */
    super(props);
    this.state = {
      index: this.props.defaultIndex
    };
  }

  render() {
    return (
      <div className="selector">
        {
          this.props.list.map((name, idx) =>
            <button
                key={idx}
                className={this.state.index === idx ? "btn btn-primary" : "btn"}
                onClick={() => {
                  if (this.state.index !== idx) {
                    this.props.onClick(name);
                    this.setState({
                      index: idx
                    });
                  }
                }}>
              {name}
            </button>
          )
        }
      </div>
    );
  }
}
