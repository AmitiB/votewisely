import React, {PureComponent} from 'react';
import * as d3 from "d3";
import {Container} from 'react-bootstrap';

export default class DevVisual2 extends PureComponent{


  constructor(props) {
    super(props);
    this.state = {
      width : 800,
      height : 500,
      bills: [],
      isLoaded: false,
      formatted_bills: {}
    };
  }

  drawBubble(){
    var data = this.state.formatted_bills;
    var width = 350;
    var height = 300;
    var format = d3.format(",d");


    var color = d3.scaleLinear()
           .domain([0, 5])
           .range(["hsl(0, 0%, 96.08%)", "  hsl(280.13, 60.63%, 49.8%)"])
           .interpolate(d3.interpolateHcl);


    const abb = pack(data);
    let focus = abb;
    let view;

    const svg = d3.select("#mysvg2");


          svg.attr("viewBox", `-${width / 2} -${height / 2} ${width} ${height}`)
          .style("display", "block")
          .style("margin", "0 -14px")
          .style("width", "calc(100% + 28px)")
          .style("height", "auto")
          .style("background", color(0))
          .style("cursor", "pointer")
          .on("click", () => zoom(abb));

    const node = svg.append("g")
          .selectAll("circle")
          .data(abb.descendants().slice(1))
          .join("circle")
          .attr("fill", d => d.children ? color(d.depth) : "white")
          .attr("pointer-events", d => !d.children ? "none" : null)
          .on("mouseover", function() { d3.select(this).attr("stroke", "#000"); })
          .on("mouseout", function() { d3.select(this).attr("stroke", null); })
          .on("click", d => focus !== d && (zoom(d), d3.event.stopPropagation()));

    const label = svg.append("g")
          .style("font", "8px sans-serif")
          .attr("font-weight", "bold")
          .attr("pointer-events", "none")
          .attr("text-anchor", "middle")
          .selectAll("text")
          .data(abb.descendants())
          .join("text")
          .style("fill-opacity", d => d.parent === abb ? 1 : 0)
          .style("display", d => d.parent === abb ? "inline" : "none")
          .text(d => d.data.name);

    zoomTo([abb.x, abb.y, abb.r * 2]);

    function zoomTo(v) {
        const k = width / v[2];

        view = v;

        label.attr("transform", d => `translate(${(d.x - v[0]) * k},${(d.y - v[1]) * k})`);
        node.attr("transform", d => `translate(${(d.x - v[0]) * k},${(d.y - v[1]) * k})`);
        node.attr("r", d => d.r * k);
    }

    function zoom(d) {

        const focus0 = focus;

        focus = d;

        const transition = svg.transition()
             .duration(d3.event.altKey ? 7500 : 750)
             .tween("zoom", d => {
              const i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2]);
              return t => zoomTo(i(t));
            });

        label.filter(function(d) { return d.parent === focus || this.style.display === "inline"; })
             .transition(transition)
             .style("fill-opacity", d => d.parent === focus ? 1 : 0)
             .on("start", function(d) { if (d.parent === focus) this.style.display = "inline"; })
             .on("end", function(d) { if (d.parent !== focus) this.style.display = "none"; });
    }

    function pack(data){
        return d3.pack()
         .size([width, height])
         .padding(10)
         (d3.hierarchy(data)
         .sum(d => d.value)
         .sort((a, b) => b.value - a.value))
    }

      return svg.node();




  }

  sortBills(data){
    var formatted = {};
    var children = [];

    formatted["name"] = "flare";
    formatted["children"] = [];

    for(var p = 0 ; p < data.length; p++){
      var issue_name = data[p].connections[0];
      if(!children.includes(issue_name)){
        children.push(issue_name);
      }
    }

    for(var l = 0; l < children.length; l++){
      var temp_dict = {};
      temp_dict["name"] = children[l];
      temp_dict["children"] = [];
      formatted.children.push(temp_dict);
    }


    for(var i = 0; i < data.length; i ++){
      var bill = data[i];
      var bill_issue = bill.connections[0];

      var temp_dict = {};
      temp_dict["name"] = bill.bill_id + ": " +bill.cosponsors.length + " cosponsors";
      temp_dict["value"] = bill.cosponsors.length;

      for(var m = 0; m < formatted.children.length; m ++){
        var t_dict = formatted.children[m];

        if(t_dict.name === bill_issue){
          t_dict.children.push(temp_dict);
          break;
        }
      }
    }

    for(var h = 0; h < formatted.children.length; h++){
      if(formatted.children[h].name === "Gender Marker Updates On Identification Documents"){
        var temp_dict_replace = {};
        temp_dict_replace["name"] = "Gender I.D. Documents"
        temp_dict_replace["children"] = formatted.children[h].children;

        formatted.children[h] = temp_dict_replace;
        break;
      }
    }
    this.setState({formatted_bills: formatted})
  }

  componentDidMount() {
      fetch("https://api.rainbowconnection.me/v1/bills/all")
      .then(res => res.json())
      .then(res => this.setState({bills: res}))
      .then(res => this.setState({isLoaded: true}))
      .then(res => this.sortBills(this.state.bills))
      .then(res => this.drawBubble());
  }


  render(){
    return (
      <div style={{background: "whitesmoke", paddingBottom: '40px', marginTop: '50px'}}>
            <br></br>
            <h5>Support for Bills</h5>
            <p>Categorizes Bills into Issues and supporting Representatives into Bills</p>
          <svg id="mysvg2" width={this.state.width} height = {this.state.height}></svg>
      </div>
    );
  }
}
