import React from 'react';
import { Link } from 'react-router-dom';
import { shallow } from 'enzyme';

import IssueTile from '../issueTile';

describe("Issue Tile", () => {
	it("renders with correct values", () => {
		const data = {
			route: "test-link",
			name: "test-name",
			icon: "test-icon",
			summary: "test-summary",
			question: "test-question",
			votes_total: 56353,
			category: "test-category",
			pct_yes: 38.3223,
			pct_no: 62.4903,
			pct_neutral: 1
		}
		const wrapper = shallow(
			<IssueTile
				data = {data}
 			/>
		);

		const links = wrapper.find(Link);
		for (var i = 0; i < links.length; i++)
			expect(links.at(i).prop('to')).toEqual("/issues/test-link");
		expect(wrapper.find('img').prop('src')).toEqual("test-icon");
		expect(wrapper.find('h2').text()).toEqual("test-name");
		expect(wrapper.find('p').at(0).text()).toEqual("Description: test-summary");
		expect(wrapper.find('p').at(1).text()).toEqual("Essential Question: test-question");
		expect(wrapper.find('p').at(2).text()).toEqual("Total Votes: 56353");
		expect(wrapper.find('p').at(3).text()).toEqual("Votes for: 38.3223%");
		expect(wrapper.find('p').at(4).text()).toEqual("Votes against: 62.4903%");
		expect(wrapper.find('p').at(5).text()).toEqual("Votes neutral: 1%");
		expect(wrapper.find('p').at(6).text()).toEqual("Category: test-category");
	});

    it("renders with correct highlighting through span", () => {
			const data = {
				route: "test-link",
				name: "test-Name",
				icon: "test-icon",
				summary: "test-summary",
				question: "test-question",
				votes_total: 56353,
				category: "test-category",
				pct_yes: 38.3223,
				pct_no: 62.4903,
				pct_neutral: 1
			}
			const wrapper = shallow(
				<IssueTile
					data = {data}
					highlight = {{
						keywords: ["summary"],
						exact: ["Name"]
					}}
	 			/>
		);

		const links = wrapper.find(Link);
		for (var i = 0; i < links.length; i++)
			expect(links.at(i).prop('to')).toEqual("/issues/test-link");
		expect(wrapper.find('img').prop('src')).toEqual("test-icon");
		expect(wrapper.find('h2').text()).toEqual("test-Name");
		expect(wrapper.find('p').at(0).text()).toEqual("Description: test-summary");
		expect(wrapper.find('p').at(1).text()).toEqual("Essential Question: test-question");
		expect(wrapper.find('p').at(2).text()).toEqual("Total Votes: 56353");
		expect(wrapper.find('p').at(3).text()).toEqual("Votes for: 38.3223%");
		expect(wrapper.find('p').at(4).text()).toEqual("Votes against: 62.4903%");
		expect(wrapper.find('p').at(5).text()).toEqual("Votes neutral: 1%");
		expect(wrapper.find('p').at(6).text()).toEqual("Category: test-category");
		expect(wrapper.find('h2').find('span').length).toEqual(1);
    expect(wrapper.find('h2').find('span').text()).toEqual('Name');
		expect(wrapper.find('p').at(0).text()).toEqual("Description: test-summary");
    expect(wrapper.find('p').find('span').length).toEqual(1);
    expect(wrapper.find('p').find('span').text()).toEqual('summary');
    });
});
