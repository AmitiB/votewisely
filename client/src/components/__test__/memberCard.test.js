import React from 'react';
import { shallow } from 'enzyme';

import MemberCard from '../memberCard';

describe('Member Card', () => {
	it('render img with link', () => {
		const wrapper = shallow(<MemberCard image="test_link"/>);
		expect(wrapper.find("img").props().src).toEqual("test_link");
	});

	it ('render details of one member from props', () => {
		const wrapper = shallow(
			<MemberCard name="test"
				job="myjob"
				class="high"
				bio="want one?"
				commits={999}
				issues={9999}
				tests={999}/>
			);
		const details_found = wrapper.find("#details");
		expect(details_found.length).toEqual(1);
		expect(details_found.find("h2").text()).toEqual("test");
		expect(details_found.find("h5").text()).toEqual("myjob");
		expect(details_found.find("p").at(0).text()).toEqual("high");
		expect(details_found.find("p").at(1).text()).toEqual("want one?");
		expect(details_found.find("span").at(0).text()).toEqual("Commits999");
		expect(details_found.find("span").at(1).text()).toEqual("Issues9999");
		expect(details_found.find("span").at(2).text()).toEqual("Tests999");
	});
});
