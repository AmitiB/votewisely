import React, {Component} from 'react';
import {Link} from "react-router-dom";
import '../css/issuePage.css';
import Banner from '../components/banner';

export default class DescriptionAttribute extends Component {

  render() {
    return (
      <div id="banner-container">
        <Banner
          text = {this.props.issue}
          img = {this.props.issueBanner}
        />
        <div class="container">
          <div class="body-sections">
            <div class="left-body">
              <div>
                <h2>Description</h2>
                <hr />
                <p>{this.props.description}</p>
                <p class="credits">Credits to <a href={this.props.descriptionWebsite} target="_blank" class="link">Wikipedia</a> and <a href="https://en.wikipedia.org/w/api.php" class="link">Wikipedia API</a></p>
              </div>

              <img src={this.props.image1} id="first-image"/>

              <div>
                <h2>Public Opinion</h2>
                <hr />
                <p>{this.props.publicOpinion}</p>
                <p class="credits">Credits to <a href={this.props.descriptionWebsite} target="_blank" class="link">Wikipedia</a> and <a href="https://en.wikipedia.org/w/api.php" class="link">Wikipedia API</a></p>
              </div>

              <img src={this.props.image2} id="first-image"/>

              <div>
                <h2>ProCon Views</h2>
                <hr />
                <p class="core-question">{this.props.coreQuestion}</p>
                <div class="yes-no">
                  <div class="yes">
                    <h2>Why yes?</h2>
                    <p>{this.props.yes}</p>
                  </div>
                  <div class="no">
                    <h2>Why not?</h2>
                    <p>{this.props.no}</p>
                  </div>
                </div>
                <p class="credits">Credits to <a href={this.props.proConWebsite} target="_blank" class="link">ProCon.org</a></p>
              </div>
            </div>
            <div class="right-body">
              <div>
                <h2 class="recent-congressional-title">Recent Congressional Statements</h2>
                <hr />
                <p><a href={this.props.articleLink1} target="_blank" class="link">{this.props.article1}</a> by {this.props.author1} on {this.props.date1} in {this.props.state1}.</p>
                <p><a href={this.props.articleLink2} target="_blank" class="link">{this.props.article2}</a> by {this.props.author2} on {this.props.date2} in {this.props.state2}.</p>
                <p><a href={this.props.articleLink3} target="_blank" class="link">{this.props.article3}</a> by {this.props.author3} on {this.props.date3} in {this.props.state3}.</p>
                <p><a href={this.props.articleLink4} target="_blank" class="link">{this.props.article4}</a> by {this.props.author4} on {this.props.date4} in {this.props.state4}.</p>
                <p><a href={this.props.articleLink5} target="_blank" class="link">{this.props.article5}</a> by {this.props.author5} on {this.props.date5} in {this.props.state5}.</p>
                <p><Link to="../pages/map">Find more info on the Map</Link></p>
                <p class="credits">Credits to <a href="https://www.congress.gov/" target="_blank" class="link">Congress.gov</a> and <a href="https://projects.propublica.org/api-docs/congress-api/" target="_blank" class="link">ProPublica API</a></p>
              </div>
              <div>
                <h2 class="recent-congressional-title">Recent Congressional Bills</h2>
                <hr />
                <p><a href={this.props.billLink1} target="_blank" class="link">{this.props.bill1}</a> from <a href={this.props.congressLink1} target="_blank" class="link">Congress.gov</a> by {this.props.representative1}.</p>
                <p><a href={this.props.billLink2} target="_blank" class="link">{this.props.bill2}</a> from <a href={this.props.congressLink2} target="_blank" class="link">Congress.gov</a> by {this.props.representative2}.</p>
                <p><a href={this.props.billLink3} target="_blank" class="link">{this.props.bill3}</a> from <a href={this.props.congressLink3} target="_blank" class="link">Congress.gov</a> by {this.props.representative3}.</p>
                <p><a href={this.props.billLink4} target="_blank" class="link">{this.props.bill4}</a> from <a href={this.props.congressLink4} target="_blank" class="link">Congress.gov</a> by {this.props.representative4}.</p>
                <p><Link to="../pages/reps">Find more info at Representatives</Link></p>
                <p class="credits">Credits to <a href="https://www.congress.gov/" target="_blank" class="link">Congress.gov</a> and <a href="https://projects.propublica.org/api-docs/congress-api/" target="_blank" class="link">ProPublica API</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
