import React, {PureComponent} from 'react';
import * as d3 from "d3";
import Slice from './slice';
import { nodelink } from '../api';
import { DropdownButton } from "react-bootstrap";
import DropdownFilter from '../components/dropdownFilter';

export default class Pie extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      stateFilter: 'AK',
    };
  this.handleStateFilter = this.handleStateFilter.bind(this);
  }

  componentDidMount() {
    fetch(nodelink + "/api/catsperstate")
      .then(res => res.json())
      .then(res => res.response)
      .then(states => {
      var statesList = [];

      for (var j = 0; j < states.length; j++) {
        statesList.push(states[j])
      }

      this.setState({
        statesList: statesList
      });
    });

    fetch(nodelink + "/api/catsperstate/" + this.state.stateFilter)
      .then(res => res.json())
      .then(res => res.response)
      .then(categories => {
        var categoryList = [];
        var data = [];

        var totalCount = 0;
        for (var k = 0; k < categories.length; k++) {
            totalCount += categories[k].count;
        }

        var minPercent = totalCount * .04;

        for (var i = 0; i < categories.length; i++) {
          if (categories[i].count >= minPercent) {
            categoryList.push(categories[i].category)
            data.push(categories[i].count)
          }
        }

        this.setState({
          categoryList : categoryList,
          data : data
        });
      });
    }

    handleStateFilter(s) {
      fetch(nodelink + "/api/catsperstate/" + s)
        .then(res => res.json())
        .then(res => res.response)
        .then(categories => {
          var categoryList = [];
          var data = [];

          var totalCount = 0;
          for (var k = 0; k < categories.length; k++) {
              totalCount += categories[k].count;
          }

          var minPercent = totalCount * .04;

          for (var i = 0; i < categories.length; i++) {
            if (categories[i].count >= minPercent) {
              categoryList.push(categories[i].category)
              data.push(categories[i].count)
            }
          }

          this.setState({
            categoryList : categoryList,
            data : data,
            stateFilter: s
          });
        });
      console.log(s)
    }

    renderStateDropdown() {
      if (this.state.statesList) {
        return (
          <DropdownButton id="dropdown-basic-button" title={"Pick a state: " + this.state.stateFilter}>
            <DropdownFilter
              list={this.state.statesList}
              onClick={this.handleStateFilter}
              defaultIndex={0} />
          </DropdownButton>
        );
      }
    }

  render() {
    if (this.state.data) {
      var data = this.state.data;
      var arcs = d3.pie()(data);
      return (
        <div className='box'>
          <h3>Percentage of Bills related to Issues by State</h3>
          {this.renderStateDropdown()}
          <svg width="60%" height="60%" viewBox="-100 -100 200 300">
           {arcs.map((obj, i) =>
             <Slice
               innerRadius = {0}
               outerRadius = {100}
               startAngle = {obj.startAngle}
               endAngle = {obj.endAngle}
               fillColor = {d3.rgb(Math.random() * (180 - 1) + 1,
                            Math.random() * (180 - 1) + 1, Math.random() * (180 - 60) + 1)}
               label = {this.state.categoryList[i]}
               id = {"pi-" + i}
             />
           )}
          </svg>
        </div>
      );
    }
    return ('Loading')
  }
}
