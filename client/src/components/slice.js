import React, { PureComponent } from 'react';
import * as d3 from "d3";
import '../css/slice.css';

export default class Pie extends PureComponent {
  render() {
    var {fillColor, innerRadius, outerRadius, startAngle, endAngle, label} = this.props;
    var arc = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .startAngle(startAngle)
      .endAngle(endAngle);

    return (
      <g>
        <path d={arc()} fill= {fillColor}/>
        <text className="slice-text"
              transform={`translate(${arc.centroid(this.props)})`}
              textAnchor="middle"
              fill="white">
          {label}
        </text>
      </g>
    );
  }
}
