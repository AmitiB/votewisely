import React, {Component} from 'react';

/* Used to render the custom cells in StateDir and Politicians_table
   Props:
      value: all of the text rendered in cells
      search: text in search box
      */
export default class hiliteCell extends Component {

  renderText(value, search, index) {
    var subIndex = value.toUpperCase().indexOf(search.toUpperCase());
    if(index === 0)
      return value.slice(0, subIndex);
    else if(index === 1)
      return value.slice(subIndex, subIndex+search.length);
    else if(index === 2)
      return value.slice(subIndex+search.length);
    else
      console.log("wrong index in hiliteCell.js, Line 19");
  }

  render() {
    /* render this one if search has input and it matches what is being searched */
    if(this.props.search !== null
      && this.props.search !==''
      && (String(this.props.value).toUpperCase().indexOf(String(this.props.search).toUpperCase())!==-1))
      return(
        <div className="hover-cursor">
          {this.renderText(String(this.props.value), String(this.props.search), 0)}
          <span className="hi-lite">{this.renderText(String(this.props.value), String(this.props.search), 1)}</span>
          {this.renderText(String(this.props.value), String(this.props.search), 2)}
        </div>
      );
    else
      return(
        <div className="hover-cursor">
          {this.props.value}
        </div>
      );
  }
}
