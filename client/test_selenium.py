import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


class GoogleTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    def testPageTitle(self):
        self.browser.get('https://thewisevote.com/')
        self.assertIn('The Wise Vote', self.browser.title)

class IssueBannerCSSTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    def testIssuesPage(self):
     self.browser.get('https://thewisevote.com/issues')
     self.assertTrue(self.browser.find_elements_by_css_selector('#banner-container'))

class AboutCSSTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    def testIssuesPage(self):
     self.browser.get('https://thewisevote.com/about')
     self.assertTrue(self.browser.find_elements_by_css_selector('#mission-statement'))

    def testContainer(self):
     self.browser.get('https://thewisevote.com/about')
     self.assertTrue(self.browser.find_element_by_id('about-container'))

class MembersCSSTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    def testIssuesPage(self):
     self.browser.get('https://thewisevote.com/about')
     self.assertTrue(self.browser.find_elements_by_css_selector('#members-list'))

class PurposeCSSTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    def testIssuesPage(self):
     self.browser.get('https://thewisevote.com/about')
     self.assertTrue(self.browser.find_elements_by_css_selector('#purpose-container'))

class HomeCSSTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    def testIssuesPage(self):
     self.browser.get('https://thewisevote.com/')
     self.assertTrue(self.browser.find_elements_by_css_selector('#splash-title'))

class MapTestCases(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    def testMapKeyCSS(self):
        self.browser.get('https://thewisevote.com/map')
        self.assertTrue(self.browser.find_elements_by_css_selector('#mapkey'))

    def testMapContainer(self):
        self.browser.get('https://thewisevote.com/map')
        self.assertTrue(self.browser.find_element_by_id('map-container'))

class AbortionCSSTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

class ClimateCSSTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    # def testIssuesPage(self):
    #  self.browser.get('https://thewisevote.com/issues/climate')
    #  self.assertTrue(self.browser.find_elements_by_css_selector('#first-image'))

class PagingTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    def testIssuesPage(self):
        self.browser.get('https://thewisevote.com/issues')
        self.assertTrue(self.browser.find_elements_by_css_selector('#paging'))

# Dynamic page - use Explicit Waits
class RepsPagesTestCase(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    # Check that a senator has a Twitter timeline
    def testSenTwitterFeed(self):
        self.browser.get('https://thewisevote.com/politicians/ted-cruz')
        self.assertTrue(EC.visibility_of_element_located((By.ID, "twitter-timeline")))

    # Check if the state listed in senator's profile is clickable
    def testSenStateLink(self):
        self.browser.get('https://thewisevote.com/politicians/ted-cruz')
        self.assertTrue(EC.element_to_be_clickable((By.ID, "stateLink")))


    def testSenRelPeople(self):
        self.browser.get('https://thewisevote.com/politicians/ted-cruz')
        self.assertTrue(EC.presence_of_element_located((By.CLASS_NAME, "RelPeopleLink")))

    # Test if the link to related people is clickable on a senator's page
    def testSenRelPeopleClick(self):
        self.browser.get('https://thewisevote.com/politicians/ted-cruz')
        self.assertTrue(EC.element_to_be_clickable((By.CLASS_NAME, "RelPeopleLink")))

    # Check if the state listed in representative's profile is clickable
    def testRepStateLink(self):
        self.browser.get('https://thewisevote.com/politicians/alma-adams')
        self.assertTrue(EC.element_to_be_clickable((By.ID, "stateLink")))

    # Test if there is a Twitter feed on a representative's page
    def testRepTwitterFeed(self):
        self.browser.get('https://thewisevote.com/politicians/alma-adams')
        self.assertTrue(EC.visibility_of_element_located((By.ID, "twitter-timeline")))

    def testRepRelPeople(self):
        self.browser.get('https://thewisevote.com/politicians/ted-cruz')
        self.assertTrue(EC.presence_of_element_located((By.CLASS_NAME, "RelPeopleLink")))

    # Test if the link to related people is clickable on a representative's page
    def testRepRelPeopleClick(self):
        self.browser.get('https://thewisevote.com/politicians/alma-adams')
        self.assertTrue(EC.element_to_be_clickable((By.CLASS_NAME, "RelPeopleLink")))

    def testSenCorrectName(self):
        self.browser.get('https://thewisevote.com/politicians/ted-cruz')
        self.assertTrue(EC.text_to_be_present_in_element((By.ID, "title"), "Ted Cruz"))

    def testRepCorrectName(self):
        self.browser.get('https://thewisevote.com/politicians/alma-adams')
        assert(EC.text_to_be_present_in_element((By.ID, "title"), "Ted Cruz"))

class StatePagesTestCases(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()
        self.addCleanup(self.browser.quit)

    def testSenLinkClickable(self):
        self.browser.get('https://thewisevote.com/states/texas')
        self.assertTrue(EC.element_to_be_clickable((By.ID, "SenButton")))

    def testRepLinkClickable(self):
        self.browser.get('https://thewisevote.com/states/texas')
        self.assertTrue(EC.element_to_be_clickable((By.ID, "RepLink")))

    # def testSenCorrectLink(self):
    #     self.browser.get('https://thewisevote.com/states/texas')
    #     name = 'Ted Cruz'
    #     try:
    #         wait = WebDriverWait(self.browser, 2)
    #         btn = wait.until(EC.element_to_be_clickable((By.TAG_NAME, "Col")))
    #          # element = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="section-newsfeed"]/div/ul[1]/li[2]/div/div/div[2]/a/span[1]/svg/path[2]'))).click()
    #         element = wait.until(EC.element_to_be_clickable((By.ID, "SenButton")))
    #         # element = EC.element_to_be_clickable((By.ID, "SenButton"))
    #         element.click()
    #         self.assertTrue(EC.text_to_be_present_in_element((By.ID, "name"), "John Cornyn"))
    #     except TimeoutException:
    #         print("WTF it should have loaded")
    #         assert False
    #     finally:
    #         self.browser.quit

    def testCorrectName(self):
        self.browser.get('https://thewisevote.com/states/florida')
        self.assertTrue(EC.text_to_be_present_in_element((By.ID, "title"), "Florida"))

    def testCorrectGovernor(self):
        self.browser.get('https://thewisevote.com/states/alabama')
        self.assertTrue(EC.text_to_be_present_in_element((By.ID, "gov"), " Governor: Kay Ivey"))

    def testCorrectCapital(self):
        self.browser.get('https://thewisevote.com/states/south-carolina')
        self.assertTrue(EC.text_to_be_present_in_element((By.ID, "capital"), " Capital: Columbia"))


# class TestPoliticianTable(unittest.TestCase):
#     def setUp(self):
#         self.browser = webdriver.Chrome()
#         self.addCleanup(self.browser.quit)


if __name__ == '__main__':
    unittest.main(verbosity=2)
