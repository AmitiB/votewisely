SHELL := /bin/bash

all: clean

pipeline-keys:
	@echo "Adding API keys for deployment"
	echo "apikey = '$$PROPUBLICA_APIKEY'" > server/database/apikey.py

api-unittest: pipeline-keys
	cd server/database && pytest

client-unittest:
	cd client && npm install && npm test

check:
	@echo "Checking if $$TYPE package.json and package-lock.json exist."
	@if [[ ! -f $$TYPE/package.json ]]; then \
		echo "$$TYPE/package.json: No such file"; \
		exit 2; \
	fi
	@if [[ ! -f $$TYPE/package-lock.json ]]; then \
		echo "$$TYPE/package-lock.json: No such file"; \
		exit 2; \
	fi
	@echo "Success, $$TYPE package.json and package-lock.json exist."

update: check
	cd $$TYPE && npm install && npm audit fix

build: update
	cd $$TYPE && CI=false npm run build #To be changed later with CI=true

env: check
	@echo "Checking if TARGET client environment needs updating"
	-scp -o StrictHostKeyChecking=no -i $$SSH_FILE ec2-user@thewisevote.com:$$TARGET/$$TYPE/package.json .
	diff -q ./$$TYPE/package.json ./package.json; \
	if [[ $$? -ne 0 ]]; then \
		echo "Changing TARGET environment"; \
		ssh -o StrictHostKeyChecking=no -i $$SSH_FILE ec2-user@thewisevote.com \
			"mkdir -p $$TARGET/$$TYPE/"; \
		scp -o StrictHostKeyChecking=no -i $$SSH_FILE $$TYPE/package.json $$TYPE/package-lock.json ec2-user@thewisevote.com:$$TARGET/$$TYPE/; \
		ssh -o StrictHostKeyChecking=no -i $$SSH_FILE ec2-user@thewisevote.com \
			"cd $$TARGET/$$TYPE/ && rm -rf node_modules && npm install && npm audit fix"; \
	fi
	rm -rf package.json

client-deploy: build env
	NODE_ENV=development pm2 start client/server.js --name test --update-env
	pm2 kill
	-ssh -o StrictHostKeyChecking=no -i $$SSH_FILE ec2-user@thewisevote.com \
		"cd $$TARGET/client && pm2 stop $$NAME && rm -rf build server.js"
	scp -o StrictHostKeyChecking=no -i $$SSH_FILE -r client/build/ client/server.js ec2-user@thewisevote.com:$$TARGET/client/
	ssh -o StrictHostKeyChecking=no -i $$SSH_FILE ec2-user@thewisevote.com \
		"cd $$TARGET && PORT=$$PORT NODE_ENV=$$ENV pm2 start client/server.js --name $$NAME --update-env"

server-deploy: env
	NODE_ENV=development pm2 start server/node/server.js --name test --update-env
	pm2 kill
	-ssh -o StrictHostKeyChecking=no -i $$SSH_FILE ec2-user@thewisevote.com \
		"cd $$TARGET/server/node && pm2 stop $$NAME && rm -rf !\(package.json\|package-lock.json\|node_modules\)"
	rsync -rv -e "ssh -o StrictHostKeyChecking=no -i $$SSH_FILE" --exclude=package.json --exclude=package-lock.json --exclude=node_modules --exclude=test server/node/ ec2-user@thewisevote.com:$$TARGET/server/node/
	ssh -o StrictHostKeyChecking=no -i $$SSH_FILE ec2-user@thewisevote.com \
		"cd $$TARGET && PORT=$$PORT NODE_ENV=$$ENV pm2 start server/node/server.js --name $$NAME --update-env"

clean:
	rm -rf client/build
